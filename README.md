
# User Guide

## for a project that is contained by projects/

see https://gitlab.com/abseboco/cc-syncme

See [projects README](./projects/README.md)

## for a project that is not contained by projects/

See [Old README.md](./old.README.md)


# Contribu Guide

* Run `luacheck projects/`