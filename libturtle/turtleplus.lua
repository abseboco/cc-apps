local api = {}

local function errorIfFalse(moved, message)
  if not moved then
    error(message)
  end
end

function api.forward(length)
  for _=1,length do
    errorIfFalse(turtle.forward())
  end
end

function api.up(length)
  for _=1,length do
    errorIfFalse(turtle.up())
  end
end

function api.back(length)
  for _=1,length do
    errorIfFalse(turtle.back())
  end
end

function api.down(length)
  for _=1,length do
    errorIfFalse(turtle.down())
  end
end

--returns the blocks travelled until hit
function api.backUntilHit()
  local length = 0
  while turtle.back() do
     length = length + 1
  end
  return length
end

function api.forwardUntilHit()
  local length = 0
  while turtle.forward() do
     length = length + 1
  end
  return length
end

function api.upUntilHit()
  local length = 0
  while turtle.up() do
     length = length + 1
  end
  return length
end

function api.downUntilHit()
  local length = 0
  while turtle.down() do
     length = length + 1
  end
  return length
end

function api.turnBackwards()
  turtle.turnLeft()
  turtle.turnLeft()
end

function api.left()
  turtle.turnLeft()
end

function api.right()
  turtle.turnRight()
end

return api
