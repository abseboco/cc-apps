local args = { ... }
local subcommand = args[1]

local version = '1.0.0'

local function printHelp()
  print('Usage:')
  print('# Print current version')
  print('dev version')
  print('# Update app and run the configured app')
  print('dev run [args]')
  print('# Update the configured app')
  print('dev <update | u> <app | a>')
  print('# Update this script')
  print('dev <update | u> <self | s>')
  print('# Update a lib or all libs from the app')
  print('dev <update | u> <lib | l> [lib]')
  print('# Delete config')
  print('dev unset')
end

local function printVersion()
  print('Version: '..version)
end

local function deleteConfig()
  settings.unset('run-dev.branch')
  settings.unset('run-dev.project')
  settings.unset('run-dev.run-script')
  settings.save()
end

local function replaceFile(url, filepath)
  local absoluteFilepath = shell.resolve(filepath)
  print('-> /'..absoluteFilepath)
  fs.delete(absoluteFilepath)
  -- can not use shell.run('wget') since it
  -- * does not return false in case of failure
  -- * logs three messages
  local request, cause1 = http.get(url)
  if not request then
    error('Request '..url..' failed: '..cause1)
  end
  local body = request.readAll()
  request.close()
  local handle, cause2 = io.open(absoluteFilepath, 'w')
  if not handle:write(body) then
    error('Unable to write: '..cause2)
  end
  handle:flush()
  handle:close()
end


local function setEnv(key, value)
  settings.set(key, value)
  settings.save()
end

local function requireUserInput(message)
  print(message..':')
  local input = read()
  if input == '' then
    error('Input can not be empty')
  end
  return input
end

local function requireUserInputWithSuggestion(message, suggestion)
  print(message..': "'..suggestion..'" Enter nothing to accept suggestion?"')
  local input = read()
  if input == '' then
    return suggestion
  else
    return input
  end
end

local function getBranch()
  local branch = settings.get('run-dev.branch')
  if not branch then
    branch = requireUserInputWithSuggestion('Enter branch name', 'main')
    setEnv('run-dev.branch', branch)
  end
  return branch
end

local function getProjectName()
  local projectName = settings.get('run-dev.project')
  if not projectName then
    projectName = requireUserInput('Enter project name')
    setEnv('run-dev.project', projectName)
  end
  return projectName
end

local function getBaseUrl()
  local branch = getBranch()
  return 'https://gitlab.com/abseboco/cc-apps/-/raw/'..branch
end

local function getProjectBaseUrl()
  local projectName = getProjectName()
  return getBaseUrl()..'/'..projectName
end

local function getScriptName()
  local runScriptName = settings.get('run-dev.run-script')
  if not runScriptName then
    local projectName = getProjectName()
    runScriptName = requireUserInputWithSuggestion('Enter script to run', projectName)
    setEnv('run-dev.run-script', runScriptName)
  end
  return runScriptName
end

local function updateSelf()
  print('--Update dev script--')
  local baseUrl = getBaseUrl()
  replaceFile(baseUrl..'/dev.lua', 'dev.lua')
end

local function updateAppInfo()
  local baseUrl = getProjectBaseUrl()
  replaceFile(baseUrl..'/appinfo.lua', 'appinfo.lua')
end

local function checkAndGetInfo(packageName)
  local info = require(packageName)
  if info == nil then
    error(packageName..' returned nil')
  end
  if info.files == nil then
    error(packageName..' returned object without any files')
  end
  return info
end

local function updateAndCheckAppInfo()
  updateAppInfo()
  return checkAndGetInfo('appinfo')
end

local function updateApp()
  print('--Update app scripts--')
  local appInfo = updateAndCheckAppInfo()
  local baseUrl = getProjectBaseUrl()
  for _, file in ipairs(appInfo.files) do
    replaceFile(baseUrl..'/'..file, file)
  end
  return appInfo
end

local function updateLib(lib)
  local baseUrl = getBaseUrl()
  -- Update libinfo
  local libBaseUrl = baseUrl..'/'..lib
  replaceFile(libBaseUrl..'/libinfo.lua', fs.combine(lib, 'libinfo.lua'))
  local libInfo = checkAndGetInfo(lib..'.libinfo')
  for _, file in ipairs(libInfo.files) do
    replaceFile(libBaseUrl..'/'..file, fs.combine(lib, file))
  end
end

local function updateLibs(appInfo)
  print('--Update libs--')
  if appInfo.libs == nil then
    print('No libs configured')
    return
  end
  for _, lib in ipairs(appInfo.libs) do
    updateLib(lib)
  end
end

local function updateLibsIfNotExisting(appInfo)
  if appInfo.libs == nil then
    return
  end
  local missingLibInfoFile = false
  for _, lib in ipairs(appInfo.libs) do
    local libInfoFile = shell.resolve(fs.combine(lib, 'libinfo.lua'))
    if not fs.exists(libInfoFile) then
      missingLibInfoFile = true
    end
  end
  if missingLibInfoFile then
    updateLibs(appInfo)
  end
end

local function runApp(appArgs)
  local runScriptName = getScriptName()
  if #(appArgs) > 0 then
    runScriptName = runScriptName..' '..table.concat(appArgs, ' ')
  end
  print('--Running app "'..runScriptName..'"--')
  shell.run(runScriptName)
end

if subcommand == 'run' then
  local appInfo = updateApp()
  updateLibsIfNotExisting(appInfo)
  local appArgs = table.pack(table.unpack(args, 2))
  runApp(appArgs)
elseif subcommand == 'version' then
  printVersion()
elseif subcommand == 'unset' then
  deleteConfig()
elseif subcommand == 'update' or subcommand == 'u' then
  local updateTarget = args[2]
  if updateTarget == 'app' or updateTarget == 'a' then
    updateApp()
  elseif updateTarget == 'self' or updateTarget == 's' then
    updateSelf()
  elseif updateTarget == 'lib' or updateTarget == 'libs' or updateTarget == 'l' then
    local libTarget = args[3]
    if libTarget then
      updateLib(libTarget)
    else
      local appInfo = updateAndCheckAppInfo()
      updateLibs(appInfo)
    end
  else
    printHelp()
  end
else
  printHelp()
end
