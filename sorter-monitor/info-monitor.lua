local api = {}

local function writeTimeProgressbar()
  local resolution = 4
  local progress = math.ceil(os.time() / 24 * resolution)
  local previousColor = monitor.getBackgroundColor()
  monitor.setBackgroundColor(colors.blue)
  for _=1,progress do
    monitor.write(' ')
  end
  monitor.setBackgroundColor(colors.lightBlue)
  for _=1,(resolution - progress) do
    monitor.write(' ')
  end
  monitor.setBackgroundColor(previousColor)
end

local function renderInfo()
  monitor.clear()
  monitor.setCursorPos(1, 1)
  monitor.write(textutils.formatTime(os.time()))
  monitor.setCursorPos(10, 1)
  writeTimeProgressbar()
  monitor.setCursorPos(1, 2)
  monitor.write(state)

  if messagePart2 then
    monitor.setCursorPos(1, 3)
    monitor.write(messagePart2)
  end
  if messagePart3 then
    monitor.setCursorPos(1, 4)
    monitor.write(messagePart3)
  end
end

local function renderPreset()
  monitor.clear()
  monitor.setCursorPos(1, 1)
  monitor.write('Presets')

  monitor.setCursorPos(1, 2)
  monitor.write('Redstone')
  monitor.blit('   ', '000', 'fee')

  monitor.setCursorPos(1, 3)
  monitor.write('Building')
  monitor.blit('   ', '000', 'fcc')
end

local function renderSwitchButton()
  local width, _ = monitor.getSize()
  monitor.setCursorPos(width, 1)
  if page == 'info' then
    monitor.blit('I', '0', 'b')
  elseif page == 'preset' then
    monitor.blit('P', '0', '1')
  else
    error('page is invalid: '..page)
  end
end

function api.render()
  if monitor == nil then
    return
  end
  if page == 'info' then
    renderInfo()
  elseif page == 'preset' then
    renderPreset()
  else
    error('page is invalid: '..page)
  end
  renderSwitchButton()
end

function api.togglePage()
  if page == 'info' then
    page = 'preset'
  elseif page == 'preset' then
    page = 'info'
  else
    error('page is invalid: '..page)
  end
end

return api
