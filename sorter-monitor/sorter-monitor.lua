local infoMonitor = require('info-monitor')
local pretty = require('cc.pretty')

local SELF_HOSTNAME = 'sorter-monitor'
local TARGET_HOSTNAME = 'sorter-turtle'
local PROTOCOL = 'absebo-sorter'
local REFRESH_INTERVAL = 10

local indexData = nil

local speaker = peripheral.find('speaker')

local modem = peripheral.find('modem')
if modem == nil then
  error('Wireless modem required: No modem present')
end
if not modem.isWireless() then
  error('Wireless modem required: No wireless modem present')
end
local modemSide = peripheral.getName(modem)
rednet.open(modemSide)
rednet.host(PROTOCOL, SELF_HOSTNAME)

local monitor = peripheral.find('monitor')

if monitor == nil then
  print('Monitor is not present')
end

local function sendMessage(message)
  local id = rednet.lookup(PROTOCOL, TARGET_HOSTNAME)
  if not id then
    print('Target host not available')
    return
  end
  print('Send message', pretty.pretty(message))
  rednet.send(id, message, PROTOCOL)
end

local function receiveMessage()
  local id, message, protocol = rednet.receive(PROTOCOL, 30)
  if id == nil then
    error('Nobody responded in time')
  end
  if protocol ~= PROTOCOL then
    error('Received message with wrong protocol')
  end
  print('Receive message', pretty.pretty(message))
  return message
end

local function splitAtSpace(msg)
   local t={}
   for str in string.gmatch(msg, "%S+") do
      table.insert(t, str)
   end
   return t
end

print('Started')
os.startTimer(REFRESH_INTERVAL)

-- info or preset
local page = 'info'
local state = '0_0'
local messagePart2 = ''
local messagePart3 = ''


local function getOrRequestIndex()
  if indexData then
    return indexData
  else
    sendMessage({
      type = 'GetIndexRequest'
    })
    local message = receiveMessage()
    if message.type ~= 'GetIndexResponse' then
      print(message.type)
      error('Received message with wrong type')
    end
    indexData = message.data
    return indexData
  end
end

local function normalizeString(str)
  return string.gsub(string.lower(str), '[:|._]', ' ')
end
-- return a list of possible matches
local function searchItemInIndex(itemToSearch)
  local itemname = normalizeString(itemToSearch)
  local indexData = getOrRequestIndex()
  local resultList = {}
  for key, _ in pairs(indexData) do
    if string.find(normalizeString(key), itemname) then
      table.insert(resultList, key)
    end
  end
  return resultList
end

local function startUserItemRequest()
  print('Item name:')
  local itemname = read()

  local possibleItems = searchItemInIndex(itemname)
  if #possibleItems == 0 then
    print('Item does not exist:', itemname)
    return
  elseif #possibleItems > 1 then
    print('Item name is ambiguous:', #possibleItems)
    for _, fullItemName in ipairs(possibleItems) do
      print(' *', fullItemName)
    end
    return
  end
  itemname = possibleItems[1]

  print('Item count: (Default: 64x ' .. itemname)
  local amount = read()

  if amount == '' then
    amount = 64
  elseif tonumber(amount) == nil then
    print('Not a valid number')
    return
  end

  print('Going to find:', amount..'x', itemname)
  sendMessage({
    type = 'GetItemsRequest',
    items = {
      {
        name = itemname,
        amount = amount
      }
    }
  })
end

local function handleKey(key)
  if key ~= keys.enter then
    print('Press ENTER to request items')
    return
  end
  startUserItemRequest()
end

local function waitAndRun()
  local eventData = {os.pullEvent()}
  local event = eventData[1]
  if event == 'rednet_message' then
    local protocol = eventData[4]
    if protocol ~= PROTOCOL then
      return
    end
    local message = eventData[3]
    print('Received message', message)
    local messageParts = splitAtSpace(message)
    state = messageParts[1]
    messagePart2 = messageParts[2]
    messagePart3 = messageParts[3]

    if state == 'Sort' then
      speaker.playSound('minecraft:block.amethyst_block.step')
    end
    infoMonitor.render()
  elseif event == 'key' then
    handleKey(eventData[2])
  elseif event == 'timer' then
    --print('timer triggered', timerId)
    infoMonitor.render()
    os.startTimer(REFRESH_INTERVAL)
  elseif event == 'monitor_touch' then
    local width, _ = monitor.getSize()
    local x = eventData[3]
    local y = eventData[4]
    if width == x and y == 1 then
      infoMonitor.togglePage()
    end
    infoMonitor.render()
  elseif event == '' then
    rednet.host(PROTOCOL, SELF_HOSTNAME)
  end
end

infoMonitor.render()
while true do
  waitAndRun()
end
