
# Farmer

* Supported crop types: wheat, beetroots, potato, carrots, pumkin, melon

# How to setup

* Input chest = Fuel chest = Magenta chest
* Output chest = Crop output = Fuel item output chest = Green chest
* Red blocks are required blocks that need to be placed. 
* These 4 red blocks span a rectangle that can be of any size.

![./setup.png](./setup.png)
