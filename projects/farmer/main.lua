local turtleplus = require('libturtle.turtleplus')

local sleepTime = 10*60

local cropToHarvestAge = {
    ["minecraft:pumpkin"] = true,
    ["minecraft:melon"] = true,
    ["minecraft:wheat"] = 7,
    ["minecraft:beetroots"] = 3,
    ["minecraft:carrots"] = 7,
    ["minecraft:potatoes"] = 7,
}

local cropToReplantItem = {
    ["minecraft:wheat"] = "minecraft:wheat_seeds",
    ["minecraft:beetroots"] = "minecraft:beetroot_seeds",
    ["minecraft:carrots"] = "minecraft:carrot",
    ["minecraft:potatoes"] = "minecraft:potato",
}

local function hasSlotItem(slot, item)
    local detail = turtle.getItemDetail(slot)
    return detail ~= nil and detail.name == item
end

local function selectAndPlace(itemToPlace)
    if hasSlotItem(nil, itemToPlace) then
        turtle.placeDown()
    else
        for slot=1,16 do
            if hasSlotItem(slot, itemToPlace) then
                turtle.select(slot)
                turtle.placeDown()
            end
        end
    end
end

local function tryHarvestDown()
    local hasBlock, info = turtle.inspectDown()
    if not hasBlock then
        return
    end
    local cropName = info.name
    local age = info.state.age
    local harvestAge = cropToHarvestAge[cropName]
    if harvestAge == true or (age ~= nil and age == harvestAge) then
        turtle.digDown()
        local replantItem = cropToReplantItem[cropName]
        if replantItem ~= nil then
            selectAndPlace(replantItem)
        end
    end
end

local function farmRow(heightOrNil)
    if heightOrNil == nil then
        local length = 0
        tryHarvestDown()
        while turtle.forward() do
            length = length + 1
            tryHarvestDown()
        end
        return length
    else
        tryHarvestDown()
        for _=1,heightOrNil do
            turtleplus.forward(1)
            tryHarvestDown()
        end
    end
end

local function findOutHowToTurn()
    turtleplus.left()
    local leftBlock = turtle.inspect()
    turtleplus.turnBackwards()
    local rightBlock = turtle.inspect()
    turtleplus.left()
    if leftBlock and rightBlock then
        error("Unable to decide where to go: Both directory are blocked")
    elseif not leftBlock and not rightBlock then
        error("Unable to decide where to go: Both directory are available")
    elseif leftBlock and not rightBlock then
        return "right"
    elseif not leftBlock and rightBlock then
        return "left"
    else
        error("Invalid state: Impossible combination")
    end
end

local function shouldTurnLeftOrRight(turnleftOrRightIfOnOppositeSide, oppositeSide)
    if oppositeSide then
        return turnleftOrRightIfOnOppositeSide
    elseif turnleftOrRightIfOnOppositeSide == "right" then
        return "left"
    else
        return "right"
    end
end

local function goBackToStart(height, width, turnLeftOrRightIfOnOppositeSide, oppositeSide)
    print("Go back to start")
    turtleplus.turnBackwards()
    turtleplus.forward(width)
    if turnLeftOrRightIfOnOppositeSide == "left" then
        turtleplus.right()
    else
        turtleplus.left()
    end
    if oppositeSide then
        turtleplus.forward(height)
    end
    turtleplus.turnBackwards()
end

local function runSimpleFarm()
    local height = farmRow()
    local width = 0
    local turnLeftOrRightIfOnOppositeSide = findOutHowToTurn()
    print("On opposite side turn:", turnLeftOrRightIfOnOppositeSide)
    local oppositeSide = true -- starter side vs opposite side
    while true do
        local leftOrRight = shouldTurnLeftOrRight(turnLeftOrRightIfOnOppositeSide, oppositeSide)
        if leftOrRight == "left" then
            turtleplus.left()
            local moved = turtle.forward()
            if not moved then
                goBackToStart(height, width, turnLeftOrRightIfOnOppositeSide, oppositeSide)
                return
            end
            turtleplus.left()
        else
            turtleplus.right()
            local moved = turtle.forward()
            if not moved then
                goBackToStart(height, width, turnLeftOrRightIfOnOppositeSide, oppositeSide)
                return
            end
            turtleplus.right()
        end
        width = width + 1
        farmRow(height)
        oppositeSide = not oppositeSide
    end
end

local function startSleeping()
    print("Start sleeping for", sleepTime)
    sleep(sleepTime)
end

local function dumpItemsIntoChest()
    print("Start dumping")
    turtleplus.turnBackwards()
    for i=1,16,1 do
        turtle.select(i)
        local didDrop, err = turtleplus.dropIntoChest()
        if not didDrop then
            if err == "No space for items" then
                printError("Chest is full")
                error("Chest is full")
            elseif err == "No chest in front of turtle" then
                printError(err)
                error(err)
            end
        end
    end
    turtleplus.turnBackwards()
    turtle.select(1)
end

local function isDownChest()
    local present, block = turtle.inspectDown()
    return present and block.name == "minecraft:chest"
end

local function isForwardChest()
    local present, block = turtle.inspect()
    return present and block.name == "minecraft:chest"
end

local function isAtNormalStartPosition()
    if not isDownChest() then
        return false
    end

    for _=1,4 do
        if isForwardChest() then
            turtleplus.turnBackwards()
            return true
        end
        turtleplus.right()
    end

    return false
end

local function makeSureTurtleIsAtStartPosition()
    if isAtNormalStartPosition() then
        print("Start position: normal")
        return
    end
    print("Start position: abnormal")
    turtleplus.forwardUntilHit()
    turtleplus.right()
    for i=1,5 do
        print("line", i)
        turtleplus.forwardUntilHit()
        turtleplus.left()
        if turtle.detect() then
            turtleplus.turnBackwards()
            if turtle.detect() then
                turtleplus.right()
                print("Found normal start position: clockwise trap")
                return
            end
        else
            turtleplus.forward(1)
            turtleplus.turnBackwards()
            print("Found normal start position: anticlockwise trap")
            return
        end
    end
    error("unable to find start position")
end

local function checkFuel()
    local ok, err = turtleplus.refuelFromChest({
        source = "down",
        destination = "back",
        fuelLevelToReach = 3000
    })
    if not ok then
        printError(err)
    end
    return ok
end

local function main()
    makeSureTurtleIsAtStartPosition()
    dumpItemsIntoChest()
    while true do
        if checkFuel() then
            turtleplus.forward(1)
            runSimpleFarm()
            turtleplus.back(1)
            dumpItemsIntoChest()
        end
        startSleeping()
    end
end

main()