
local ITEMS_SMELTABLE_BY_ONE_COAL = 8
local FULL_STACK_OF_ITEMS = 64
local FURNANCE_SMELT_SLOT = 1
local FURNANCE_FUEL_SLOT = 2
local FURNANCE_OUTPUT_SLOT = 3

local CHECK_EVERY_SECONDS = 30

local function getChestsAndFurnances()
    local furnaceList = {}
    local barrels = {}
    local peripherals = peripheral.getNames()
    for _, peri in ipairs(peripherals) do
        if peripheral.hasType(peri, "minecraft:furnace") then
            table.insert(furnaceList, peripheral.wrap(peri))
        elseif peripheral.hasType(peri, "minecraft:barrel") then
            table.insert(barrels, peripheral.wrap(peri))
        end
    end
    return {
        fuelChest = barrels[1],
        inputChest = barrels[2],
        outputChest = barrels[3],
        furnaceList = furnaceList
    }
end

local function moveItems(options)
    local sourceInventory = options.sourceInventory
    local targetInventory = options.targetInventory
    local fromSlot = options.fromSlot
    local count = options.count
    local toSlot = options.toSlot
    return sourceInventory.pushItems(peripheral.getName(targetInventory), fromSlot, count, toSlot)
end

local function fuelFurnaces(peris)
    print("Refuel")
    for _, furnace in ipairs(peris.furnaceList) do
        for slot, _ in pairs(peris.fuelChest.list()) do
            local count = moveItems({
                sourceInventory = peris.fuelChest,
                targetInventory = furnace,
                fromSlot = slot,
                count = FULL_STACK_OF_ITEMS,
                toSlot = FURNANCE_FUEL_SLOT
            })
            if count > 0 then
                print("moved items")
            end
        end
    end
end

local function putItemsIntoFurnace(peris, furnace)
    for slot, _ in pairs(peris.inputChest.list()) do
        local count = moveItems({
            sourceInventory = peris.inputChest,
            targetInventory = furnace,
            fromSlot = slot,
            count = ITEMS_SMELTABLE_BY_ONE_COAL,
            toSlot = FURNANCE_SMELT_SLOT
        })
        if count > 0 then
            print("moved items")
            return
        end
    end
end

local function putItemsIntoFurnaces(peris)
    print("Split items")
    for _, furnace in ipairs(peris.furnaceList) do
        putItemsIntoFurnace(peris, furnace)
    end
end

local function putProcessedItemsIntoOutputChest(peris)
    print("Output items")
    for _, furnace in ipairs(peris.furnaceList) do
        local count = moveItems({
            sourceInventory = furnace,
            targetInventory = peris.outputChest,
            fromSlot = FURNANCE_OUTPUT_SLOT
        })
        if count > 0 then
            print("moved items")
        end
    end
end

local function processItems(peris)
    print("Process items")
    fuelFurnaces(peris)
    putItemsIntoFurnaces(peris)
    putProcessedItemsIntoOutputChest(peris)
end

local function requireBarrel(name, barrelOrNil)
    local nameOfBarrel = "not found"
    if barrelOrNil ~= nil then
        nameOfBarrel = peripheral.getName(barrelOrNil)
    end
    print(name, "chest:", nameOfBarrel)
    if barrelOrNil == nil then
        error("Three barrels required. Missing barrel for: "..name)
    end
end

local function printAndCheckPeris(peris)
    print("Furnaces:", table.getn(peris.furnaceList))
    requireBarrel("Fuel", peris.fuelChest)
    requireBarrel("Input", peris.inputChest)
    requireBarrel("Output", peris.outputChest)

    if table.getn(peris.furnaceList) == 0 then
        error("No furnace found")
    end
end

local function main()
    local peris = getChestsAndFurnances()
    printAndCheckPeris(peris)
    processItems(peris)
    while true do
        print("Wait")
        os.startTimer(CHECK_EVERY_SECONDS)
        local event = os.pullEvent()
        if event == "redstone" or event == "timer" then
            processItems(peris)
        end
    end
end

main()