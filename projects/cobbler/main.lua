
local function isThereSpaceLeft()
    for slot=1,16,1 do
        if turtle.getItemSpace(slot) > 0 then
            return true
        end
    end
    return false
end

print("Start digging")
local sleepAfterNoSpaceLeftInSeconds = 300
while true do
    if isThereSpaceLeft() then
        turtle.dig()
        sleep(1)
    else
        sleep(sleepAfterNoSpaceLeftInSeconds)
    end
end


