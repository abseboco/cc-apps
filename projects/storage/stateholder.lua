
local function emptyStats()
    return {
        items = {
            count = 0,
            max = "-"
        },
        slots = {
            used = 0, -- at least one item and at least one free slot for an item
            full = 0, -- slots with 64 items (maxCount would require a call to getItemDetails)
            all = 0
        },
        chests = {
            used = 0, -- at least one free slot and at least one slot filled with an item
            full = 0, -- no free slots left (but possible that more items could fit in)
            all = 0
        }
    }
end

-- itemToInventories = {"minecraft:stone" = {count = 23, 1 = "minecraft:chest_20", 2 = "minecraft:chest_21"}}
local itemToInventories = nil
-- state = "idle" | "indexing" | "sorting"
local state = ""
-- peripherals
local peris = nil

local api = {
    itemToInventories = itemToInventories,
    stats = emptyStats(),
    state = state,
    peris = peris
}

local function resetStats()
    api.stats = emptyStats()
end

api.resetStats = resetStats

return api