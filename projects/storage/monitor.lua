local stateholder = require("stateholder")

local monitor = peripheral.find("monitor")
if monitor == nil then
    print("Monitor not present")
else
    print("Monitor present:", peripheral.getName(monitor))
end

local function barGraph(data)
    local width, _ = monitor.getSize()
    local graphWidth = width - 2
    local full = math.ceil(data.full / data.all * graphWidth)
    local used = math.ceil(data.used / data.all * graphWidth)
    local free = graphWidth - full - used

    monitor.setBackgroundColor(colors.red)
    monitor.write(string.rep(" ", full))
    monitor.setBackgroundColor(colors.orange)
    monitor.write(string.rep(" ", used))
    monitor.setBackgroundColor(colors.green)
    monitor.write(string.rep(" ", free))
    monitor.setBackgroundColor(colors.black)
end

local function updateStats()
    if monitor == nil then
        return
    end
    local stats  = stateholder.stats
    monitor.clear()
    monitor.setCursorPos(2, 2)
    monitor.write("State: "..stateholder.state)
    monitor.setCursorPos(2, 3)
    monitor.write("Items: "..stats.items.count.."/"..stats.items.max)
    monitor.setCursorPos(2, 4)
    monitor.write("Slots: "..stats.slots.full.."/"..stats.slots.used.."/"..stats.slots.all)
    monitor.setCursorPos(2, 5)
    barGraph(stats.slots)
    monitor.setCursorPos(2, 6)
    monitor.write("Chests: "..stats.chests.full.."/"..stats.chests.used.."/"..stats.chests.all)
    monitor.setCursorPos(2, 7)
    barGraph(stats.chests)
end

return {
    updateStats = updateStats
}