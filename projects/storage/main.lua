local monitor = require("monitor")
local inventoryService = require("inventoryService")
local searcher = require("searcher")
local stateholder = require("stateholder")
local configurator = require("configurator")

-- Update stats
--    to show them to the user
--    to know where to sort items
local UPDATE_STATS_INVERVAL = 180

local function changeState(newState)
    stateholder.state = newState
    print("State:", stateholder.state)
    monitor.updateStats()
end

local function rememberItemIsStoredInStorage(storage, item)
    local listOfStorages = stateholder.itemToInventories[item.name]
    if listOfStorages == nil then
        listOfStorages = {count = 0}
        stateholder.itemToInventories[item.name] = listOfStorages
    end
    listOfStorages.count = listOfStorages.count + item.count
    table.insert(listOfStorages, peripheral.getName(storage))
end

local function newItemsToInventories()
    local config = configurator.get()
    local result = {}
    for itemName, inventories in pairs(config.storage.itemMapping) do
        result[itemName] = {
            count = 0,
            table.unpack(inventories)
        }
    end
    return result
end

local function retrieveStats()
    changeState("indexing")
    local start = os.time("local")
    stateholder.resetStats()
    local stats = stateholder.stats
    stateholder.itemToInventories = newItemsToInventories()
    for _, storage in ipairs(stateholder.peris.storageList) do
        local slotCount = storage.size()
        local items = storage.list()
        local usedSlotCount = table.getn(items)
        for _, item in pairs(items) do
            stats.items.count = stats.items.count + item.count
            if item.count == 64 then
                stats.slots.full = stats.slots.full + 1
            else
                stats.slots.used = stats.slots.used + 1
            end
            rememberItemIsStoredInStorage(storage, item)
        end
        stats.slots.all = stats.slots.all + slotCount
        stats.chests.all = stats.chests.all + 1
        if slotCount == usedSlotCount then
            stats.chests.full = stats.chests.full + 1
        elseif usedSlotCount > 0 then
            stats.chests.used = stats.chests.used + 1
        end
    end
    local diff = os.time("local") - start
    print("ind:", diff*60*60, "sec")
end

local function putItemsIntoOutputChests(inputChest, slot, outputList)
    for _, chest in ipairs(outputList) do
        inventoryService.moveItems({
            sourceInventory = inputChest,
            targetInventory = chest,
            fromSlot = slot
        })
    end
end

local function unableToStore(inputChest, slot)
    printError("-> unable to store")
    putItemsIntoOutputChests(inputChest, slot, stateholder.peris.unsortableList)
end

local function storeShulker(inputChest, slot)
    print("-> putting shulker in output chest")
    putItemsIntoOutputChests(inputChest, slot, stateholder.peris.outputList)
end

local function storeItem(inputChest, inventories, slot, countToStore)
    for _, inv in ipairs(inventories) do
        local count = inventoryService.moveItems({
            sourceInventory = inputChest,
            targetInventory = peripheral.wrap(inv),
            fromSlot = slot
        })
        countToStore = countToStore - count
        if countToStore == 0 then
            return true
        end
    end
    return false
end

local function sortItem(inputChest, slot, item, lastSlot)
    if slot == lastSlot then
        return
    end
    print(string.format("Sort %dx %s (%d)", item.count, item.name, slot))
    if string.find(item.name, "minecraft:[%a_]*shulker_box") then
        storeShulker(inputChest, slot)
        return
    end

    local inventories = stateholder.itemToInventories[item.name]
    if inventories == nil then
        unableToStore(inputChest, slot)
    else
        local countToStore = item.count
        local ableToStoreAllStacks = storeItem(inputChest, inventories, slot, countToStore)
        if not ableToStoreAllStacks then
            unableToStore(inputChest, slot)
        end
    end
end

local function sortAllItems()
    changeState("sorting")
    for _, inputChest in ipairs(stateholder.peris.inputList) do
        local lastSlot = inputChest.size()
        for slot, item in pairs(inputChest.list()) do
            sortItem(inputChest, slot, item, lastSlot)
        end
    end
end

local function idle()
    changeState("idle")
end

local function runAllActions()
    retrieveStats()
    sortAllItems()
    idle()
end


local function main()
    changeState("starting")
    stateholder.peris = inventoryService.waitForAllInventoriesToBePresent()
    runAllActions()
    local lastTimerId = os.startTimer(UPDATE_STATS_INVERVAL)
    while true do
        local event, timerId = os.pullEvent()
        if event == "redstone" and not redstone.getInput("back") then
            print("event:redstone")
            sortAllItems()
            idle()
        elseif event == "timer" and timerId == lastTimerId then
            print("event:timer:", lastTimerId, ":", timerId)
            runAllActions()
            lastTimerId = os.startTimer(UPDATE_STATS_INVERVAL)
        elseif event == "monitor_touch" then
            print("event:monitor_touch")
            runAllActions()
        elseif event == "key_up" then
            local keycode = timerId
            local key = keys.getName(keycode)
            if searcher.isLetter(keycode) then
                changeState("search")
                searcher.enterSearchMode(key)
                idle()
            end
        end
    end
end

main()