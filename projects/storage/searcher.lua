local inventoryService = require("inventoryService")
local stateholder = require("stateholder")

local function isLetter(keycode)
    return keys.a <= keycode and keycode <= keys.z
end

local function isNumberKeycode(keycode)
    return keys.zero <= keycode and keycode <= keys.nine
end

local function searchForItem(searchTerm)
    local foundItems = {}
    for itemName, inventories in pairs(stateholder.itemToInventories) do
        local start, stop = string.find(itemName, searchTerm)
        if start then
            local nameLength = string.len(itemName)
            local bgStart = string.rep("f", start-1)
            local bgMid = string.rep("3", stop-start+1)
            local bgEnd = string.rep("f", nameLength - stop)
            table.insert(foundItems, {
                name = itemName,
                nameLength = nameLength,
                count = inventories.count,
                textColor = string.rep("0", nameLength),
                backgroundColor = bgStart..bgMid..bgEnd,
            })
        end
    end
    table.sort(foundItems, function (k1, k2) return k1.nameLength < k2.nameLength end)
    return foundItems
end

local function retrieveItem(itemName, count)
    print("Retrieve item", itemName)
    local inventories = stateholder.itemToInventories[itemName]
    for _, inventoryName in ipairs(inventories) do
        local inventory = peripheral.wrap(inventoryName)
        for slot, item in pairs(inventory.list()) do
            if item.name == itemName then
                for _, outputChest in ipairs(stateholder.peris.outputList) do
                    local countMoved = inventoryService.moveItems({
                        sourceInventory = inventory,
                        targetInventory = outputChest,
                        fromSlot = slot,
                        count = count
                    })
                    print("countMoved", countMoved)

                    count = count - countMoved
                    if count <= 0 then
                        print("Retrieved item")
                        return
                    end
                end
            end
        end
    end
    printError("Unable to retrieve", itemName)
end

local function simpleBlit(text, textColor, backgroundColor)
    local length = string.len(text)
    term.blit(text, string.rep(textColor, length), string.rep(backgroundColor, length))
end

local function enterSearchMode(firstChar)
    local searchTerm = firstChar
    local itemCount = 0
    local foundItems = {}
    local foundItemsIndex = 1
    local enterPressed = false
    local searchOngoing = true
    local resetIndexAndCount = false
    while searchOngoing do
        --do search
        foundItems = searchForItem(searchTerm)
        --update ui
        term.clear()
        term.setCursorPos(1, 1)
        term.write("Search: "..searchTerm)
        term.setCursorPos(1, 2)
        term.write("Count: "..itemCount)
        local foundItemCount = table.getn(foundItems)
        if foundItemCount == 0 then
            term.setCursorPos(1, 3)
            simpleBlit("No items found", "e", "f")
        else
            for i, foundItem in ipairs(foundItems) do
                local isSelected = i == foundItemsIndex
                term.setCursorPos(1, 2+i)
                local textColor = foundItem.textColor
                if isSelected then
                    textColor = string.rep("1", string.len(foundItem.name))
                end
                term.blit(foundItem.name, textColor, foundItem.backgroundColor)
                simpleBlit(" "..foundItem.count, "d", "f")
            end
        end
        --input
        local _, keycode = os.pullEvent("key_up")
        local key = keys.getName(keycode)
        if isLetter(keycode) then
            searchTerm = searchTerm..key
            resetIndexAndCount = true
        elseif isNumberKeycode(keycode) then
            local keyNumber = keycode - keys.zero
            itemCount = tonumber(tostring(itemCount)..keyNumber)
        elseif keycode == keys.up then
            foundItemsIndex = math.max(1, foundItemsIndex - 1)
        elseif keycode == keys.down then
            foundItemsIndex = math.min(foundItemCount, foundItemsIndex + 1)
        elseif keycode == keys.left then
            itemCount = math.max(0, itemCount - 64)
        elseif keycode == keys.right then
            itemCount = itemCount + 64
        elseif keycode == keys.space then
            searchTerm = searchTerm.." "
            resetIndexAndCount = true
        elseif keycode == keys.period then
            searchTerm = searchTerm..":"
            resetIndexAndCount = true
        elseif keycode == keys.minus then
            searchTerm = searchTerm.."_"
            resetIndexAndCount = true
        elseif keycode == keys.backspace then
            local lengthOfSearchTerm = string.len(searchTerm)
            if lengthOfSearchTerm <= 1 then
                searchOngoing = false
            else
                searchTerm = searchTerm:sub(1, lengthOfSearchTerm - 1)
                resetIndexAndCount = true
            end
        elseif keycode == keys.enter then
            enterPressed = true
            searchOngoing = false
        end
        -- reset
        if resetIndexAndCount then
            foundItemsIndex = 1
            itemCount = 0
            resetIndexAndCount = false
        end
    end
    term.clear()
    term.setCursorPos(1, 1)
    if enterPressed and table.getn(foundItems) ~= 0 then
        local fountItemName = foundItems[foundItemsIndex].name
        retrieveItem(fountItemName, itemCount)
    end
end

return {
    isLetter = isLetter,
    enterSearchMode = enterSearchMode
}