local configurator = require("configurator")

local WAIT_BETWEEN_RESCAN_RETRIES = 30

local function getInventories()
    print("Analyzing inventories")
    local config = configurator.get()
    local inputList = {}
    local outputList = {}
    local unsortableList = {}
    local storageList = {}
    local namesOfOtherInventories = {}
    local peripherals = {peripheral.find("inventory")}
    -- automatically find inventories
    for _, wrapped in ipairs(peripherals) do
        local itemInLastSlot = wrapped.getItemDetail(wrapped.size())
        local displayName = nil
        if itemInLastSlot ~= nil then
            displayName = itemInLastSlot.displayName
        end
        if displayName == config.input.markerItemName then
            table.insert(inputList, wrapped)
        elseif displayName == config.output.markerItemName then
            table.insert(outputList, wrapped)
        elseif displayName == config.unsortable.markerItemName then
            table.insert(unsortableList, wrapped)
        else
            namesOfOtherInventories[peripheral.getName(wrapped)] = true
        end
    end
    local result = {
        inputList = inputList,
        outputList = outputList,
        unsortableList = unsortableList,
        storageList = storageList,
    }
    -- add inventories that have been explicitly configured
    for inventoryType, inventoryConfig in pairs(config) do
        for _, invName in ipairs(inventoryConfig.names) do
            local wrapped = peripheral.wrap(invName)
            if wrapped == nil then
                printError("Unable to find inventory:", invName)
            else
                print("Explicitly add", invName, "as", inventoryType)
                local list = result[inventoryType.."List"]
                table.insert(list, wrapped)
                namesOfOtherInventories[invName] = false
            end
        end
    end
    -- add all unused inventories as Storage inventories
    for name, usableAsStorage in pairs(namesOfOtherInventories) do
        if usableAsStorage then
            table.insert(storageList, peripheral.wrap(name))
        end
    end
    return result
end

local function printInventories(name, listOfChest, count, expectedCount)
    term.write(name..": ")
    if expectedCount ~= -1 then
        term.setTextColor(colors.green)
    else
        term.setTextColor(colors.yellow)
    end
    if count == 1 then
        term.write(peripheral.getName(listOfChest[1]))
    else
        term.write(count)
    end
    print("")
    term.setTextColor(colors.white)
end

local function requireChests(name, listOfChest, itemTypeConfig, result)
    local count = table.getn(listOfChest)
    local expectedCount = itemTypeConfig.count
    if expectedCount == 0 then
        printError("Critical error: requires config change")
        error("Expected count must be either -1 or a value above 0 but never 0: "..name)
    end
    printInventories(name, listOfChest, count, expectedCount)
    if expectedCount > 0 then
        if expectedCount > count then
            result.ok = false
            local message = "There are less '%s'-inventories than expected: Current: %d Expected: %d"
            printError(string.format(message, name, count, expectedCount))
        elseif expectedCount < count then
            printError("Critical error: requires config change")
            local message = "There are more '%s'-inventories than expected: Current: %d Expected: %d"
            error(string.format(message, name, count, expectedCount))
        end
    elseif count == 0 then
        local markerItemName = itemTypeConfig.markerItemName
        local errorMessage
        if markerItemName == nil then
            local message = "Put no marker item in the last slot of "
            message = message.."your chest/barrel to mark it as an '%s'-inventory"
            errorMessage = string.format(message, name)
        else
            local message = "Put an item renamed to '%s' in the last "
            message = message.."slot of your chest/barrel to mark it as an '%s'-inventory. "
            message = message.."This might be a chunk loading issue. "
            message = message.."Try to explicitly configure the count of this type. "
            message = message.."If you did everything right the number "
            message = message.."of found inventories for that type will turn green."
            errorMessage = string.format(message, markerItemName, name)
        end
        error(errorMessage)
    end
end

local function printAndCheckInventoryCount(peris)
    local config = configurator.get()
    local result = {ok = true}
    requireChests("Input", peris.inputList, config.input, result)
    requireChests("Output", peris.outputList, config.output, result)
    requireChests("Unsortable", peris.unsortableList, config.unsortable, result)
    requireChests("Storage", peris.storageList, config.storage, result)
    if result.ok then
        return true
    end
end

local function waitForAllInventoriesToBePresent()
    while true do
        local peris = getInventories()
        if printAndCheckInventoryCount(peris) then
            return peris
        end
        print("Waiting", WAIT_BETWEEN_RESCAN_RETRIES, "seconds until next scan")
        sleep(WAIT_BETWEEN_RESCAN_RETRIES)
    end
end

local function moveItems(options)
    local sourceInventory = options.sourceInventory
    local targetInventory = options.targetInventory
    local fromSlot = options.fromSlot
    local count = options.count
    local toSlot = options.toSlot
    return sourceInventory.pushItems(peripheral.getName(targetInventory), fromSlot, count, toSlot)
end

return {
    moveItems = moveItems,
    waitForAllInventoriesToBePresent = waitForAllInventoriesToBePresent
}