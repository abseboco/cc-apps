local api = {}

local CONFIG_FILE_PATH = "storage.conf"

local function emptyConfig()
    return {
        input = {
            count = -1,
            names = {},
            markerItemName = "SorterInput"
        },
        output = {
            count = -1,
            names = {},
            markerItemName = "SorterOutput"
        },
        unsortable = {
            count = -1,
            names = {},
            markerItemName = "SorterUnsortable"
        },
        storage = {
            count = -1,
            names = {},
            itemMapping = {}
        }
    }
end

local function save(config)
    print("Save config")
    local str = textutils.serialise(config)

    local file = fs.open(CONFIG_FILE_PATH, "w")
    file.write(str)
    file.close()
end

local function load()
    local file = fs.open(CONFIG_FILE_PATH, "r")
    if file == nil then
        print("Config not present")
        return nil
    end
    print("Loaded config")
    local contents = file.readAll()
    file.close()

    local data = textutils.unserialize(contents)
    if data == nil then
        error("Config file is invalid: "..CONFIG_FILE_PATH)
    end
    return data
end

local function get()
    if api.config == nil then
        local config = load()
        if config == nil then
            save(emptyConfig())
            config = load()
        end
        api.config = config
    end
    return api.config
end

api.config = nil
api.get = get
api.load = load
api.save = save

return api