local api = {}

local function errorIfFalse(moved, message)
  if not moved then
    error(message)
  end
end

function api.forward(length)
  for _=1,length do
    errorIfFalse(turtle.forward())
  end
end

function api.up(length)
  for _=1,length do
    errorIfFalse(turtle.up())
  end
end

function api.back(length)
  for _=1,length do
    errorIfFalse(turtle.back())
  end
end

function api.down(length)
  for _=1,length do
    errorIfFalse(turtle.down())
  end
end

--returns the blocks travelled until hit
function api.backUntilHit()
  local length = 0
  while turtle.back() do
     length = length + 1
  end
  return length
end

function api.forwardUntilHit()
  local length = 0
  while turtle.forward() do
     length = length + 1
  end
  return length
end

function api.upUntilHit()
  local length = 0
  while turtle.up() do
     length = length + 1
  end
  return length
end

function api.downUntilHit()
  local length = 0
  while turtle.down() do
     length = length + 1
  end
  return length
end

function api.turnBackwards()
  turtle.turnLeft()
  turtle.turnLeft()
end

function api.left()
  turtle.turnLeft()
end

function api.right()
  turtle.turnRight()
end

function api.dropIntoChest(itemCount)
  local hasBlock, info = turtle.inspect()
  if not hasBlock or info.name ~= "minecraft:chest" then
    return false, "No chest in front of turtle"
  end
  return turtle.drop(itemCount)
end

local function makeSureFacingSide(side)
  if side == "left" then
    api.left()
    return "right"
  elseif side == "right" then
    api.right()
    return "left"
  elseif side == "back" then
    api.turnBackwards()
    return "back"
  else
    -- side = front,up,down
    return "front"
  end
end

local function makeSureFacingSides(sideA, sideB)
  if sideA == sideB then
    return makeSureFacingSide(sideA)
  end
  local sideToRevertA = makeSureFacingSide(sideA)
  local sideToRevertB = makeSureFacingSide(sideB)
  if sideToRevertA ~= "front" and sideToRevertB ~= "front" then
    makeSureFacingSide(sideToRevertA)
    makeSureFacingSide(sideToRevertB)
    error("Side combination not supported:"..sideA.." and "..sideB)
  elseif sideToRevertA == "front" and sideToRevertB == "front" then
    return sideToRevertA
  elseif sideToRevertA == "front" and sideToRevertB ~= "front" then
    return sideToRevertB
  elseif sideToRevertA ~= "front" and sideToRevertB == "front" then
    return sideToRevertA
  end
  error("All cases should be handled in an if statement")
end

local function suckFromSide(itemCount, side)
  if side == "up" then
    return turtle.suckUp(itemCount)
  elseif side == "down" then
    return turtle.suckDown(itemCount)
  else
    return turtle.suck(itemCount)
  end
end

local function dropToSide(itemCount, side)
  if side == "up" then
    return turtle.dropUp(itemCount)
  elseif side == "down" then
    return turtle.dropDown(itemCount)
  else
    return turtle.drop(itemCount)
  end
end

-- not that a side combination that needs excessive turning is not supported
-- example: left and right
function api.refuelFromChest(params)
  if params == nil then
    error("turtleplus.refuelFromChest called with nil as arg")
  end
  params.source = params.source or "down"
  params.destination = params.destination or "back"
  params.fuelLevelToReach = params.fuelLevelToReach or 1000

  print("Fuel: "..turtle.getFuelLevel().."/"..params.fuelLevelToReach)
  if params.fuelLevelToReach > turtle.getFuelLevel() then
    local sideToRevert = makeSureFacingSides(params.source, params.destination)
    while params.fuelLevelToReach > turtle.getFuelLevel() do
      local oneItem = 1
      local couldSuck, _ = suckFromSide(oneItem, params.source)
      if couldSuck == false then
        makeSureFacingSide(sideToRevert)
        return false, "Fuel chest is empty"
      end
      local couldRefuel = turtle.refuel()
      if couldRefuel == false then
        makeSureFacingSide(sideToRevert)
        return false, "Fuel chest contains non fuel items"
      end
      dropToSide(oneItem, params.destination)
    end
    makeSureFacingSide(sideToRevert)
    print("Refuel finished:", turtle.getFuelLevel())
  end
  return true, nil
end

return api
