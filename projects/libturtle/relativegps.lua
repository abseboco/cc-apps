local turtleplus = require('libturtle.turtleplus')

local SIDES = {'x+', 'z+', 'x-', 'z-'}

local function getSideIfTurnedRight(startSide, rightTurns)
  for position, side in ipairs(SIDES) do
    if side == startSide then
      local positionOfNewSide = (position + rightTurns - 1) % 4 + 1
      return SIDES[positionOfNewSide]
    end
  end
  error('Side is invalid: '..startSide)
end

local RelativeGps = {}

-- This is an alternative to building a gps tower and using real gps
-- This api wraps the turtle and turtleplus api to track where the turtle has moved
-- The start position is 0,0,0 (x,y,z) (you can adjust the start position)
-- x = forward at start position (equals to a relative minecraft x or z coordinate)
-- y = height (equals to a relative minecraft y coordinate)
-- z = right at start position (equals to a relative minecraft x or z coordinate)
-- Do not use normal turtle api while using this
--
--   x+
-- z-  z+
--   x-
--
-- obj can have the following properties: x, y, z, facing, trackOperations
function RelativeGps:new(obj)
  if obj == nil then
    error("Called with nil arg: RealitveGps:new(nil)")
  end
  local o = {
    x = obj.x or 0,
    y = obj.y or 0,
    z = obj.z or 0,
    facing = obj.facing or 'x+',
    trackOperations = obj.trackOperations,
    undoOperations = {}
  }
  getSideIfTurnedRight(o.facing, 0)
  if type(o.trackOperations) ~= 'boolean' then
    o.trackOperations = false
  end

  o.startX = o.x
  o.startY = o.y
  o.startZ = o.z
  o.startFacing = o.facing

  setmetatable(o, self)
  self.__index = self
  return o
end

function RelativeGps:turnTo(side)
  if self.facing == side then
    return
  elseif getSideIfTurnedRight(self.facing, 2) == side then
    self:turnAround()
    return
  elseif getSideIfTurnedRight(self.facing, 1) == side then
    self:right()
    return
  elseif getSideIfTurnedRight(self.facing, 3) == side then
    self:left()
    return
  else
    error('Unable to turn to side: '..side)
  end
end

function RelativeGps:goTo(x, y, z)
  local diffX = 0
  local diffY = 0
  local diffZ = 0
  if x then
    diffX = x - self.x
  end
  if y then
    diffY = y - self.y
  end
  if z then
    diffZ = z - self.z
  end
  self:goToRelative(diffX, diffY, diffZ)
end

function RelativeGps:goToRelative(diffX, diffY, diffZ)
  -- diffY
  if diffY > 0 then
    self:up(diffY)
  else
    self:down(math.abs(diffY))
  end
  -- diffX
  if diffX > 0 then
    if self.facing == 'x+' then
      self:forward(diffX)
    elseif self.facing == 'x-' then
      self:back(diffX)
    else
      self:turnTo('x+')
      self:forward(diffX)
    end
  elseif diffX < 0 then
    local absDiffX = math.abs(diffX)
    if self.facing == 'x-' then
      self:forward(absDiffX)
    elseif self.facing == 'x+' then
      self:back(absDiffX)
    else
      self:turnTo('x-')
      self:forward(absDiffX)
    end
  end
  -- diffZ
  if diffZ > 0 then
    if self.facing == 'z+' then
      self:forward(diffZ)
    elseif self.facing == 'z-' then
      self:back(diffZ)
    else
      self:turnTo('z+')
      self:forward(diffZ)
    end
  elseif diffZ < 0 then
    local absDiffZ = math.abs(diffZ)
    if self.facing == 'z-' then
      self:forward(absDiffZ)
    elseif self.facing == 'z+' then
      self:back(absDiffZ)
    else
      self:turnTo('z-')
      self:forward(absDiffZ)
    end
  end
end

function RelativeGps:prependUndo(func, fuel)
  if self.trackOperations then
    table.insert(self.undoOperations, 1, {
      func = func,
      fuel = fuel
    })
  end
end

function RelativeGps:reverseOperations()
  for _, undo in ipairs(self.undoOperations) do
    undo.func()
  end
  self.x = self.startX
  self.y = self.startY
  self.z = self.startZ
  self.facing = self.startFacing
end

function RelativeGps:getFuelCostOfReverseOperations()
  local sum = 0
  for _, undo in ipairs(self.undoOperations) do
    sum = sum + undo.fuel
  end
  return sum
end

-- clones everything except start values and operations
function RelativeGps:checkpoint()
  return RelativeGps:new({
    x = self.x,
    y = self.y,
    z = self.z,
    facing = self.facing,
    trackOperations = self.trackOperations
  })
end

function RelativeGps:printPosition()
  io.write(self.x..'|'..self.y..'|'..self.z..' '..self.facing..'\n')
end

function RelativeGps:forward(length)
  if self.facing == 'x+' then
    self.x = self.x + length
  elseif self.facing == 'z-' then
    self.z = self.z - length
  elseif self.facing == 'x-' then
    self.x = self.x - length
  elseif self.facing == 'z+' then
    self.z = self.z + length
  else
    error('Invalid facing direction:'..self.facing)
  end
  turtleplus.forward(length)
  self:prependUndo(function() turtleplus.back(length) end, length)
end

function RelativeGps:back(length)
  if self.facing == 'x+' then
    self.x = self.x - length
  elseif self.facing == 'z-' then
    self.z = self.z + length
  elseif self.facing == 'x-' then
    self.x = self.x + length
  elseif self.facing == 'z+' then
    self.z = self.z - length
  else
    error('Invalid facing direction:'..self.facing)
  end
  turtleplus.back(length)
  self:prependUndo(function() turtleplus.forward(length) end, length)
end

function RelativeGps:down(length)
  self.y = self.y - length
  turtleplus.down(length)
  self:prependUndo(function() turtleplus.up(length) end, length)
end

function RelativeGps:up(length)
  self.y = self.y + length
  turtleplus.up(length)
  self:prependUndo(function() turtleplus.down(length) end, length)
end

function RelativeGps:left()
  self.facing = getSideIfTurnedRight(self.facing, 3)
  turtleplus.left()
  self:prependUndo(function() turtleplus.right() end, 0)
end

function RelativeGps:right()
  self.facing = getSideIfTurnedRight(self.facing, 1)
  turtleplus.right()
  self:prependUndo(function() turtleplus.left() end, 0)
end

function RelativeGps:turnAround()
  self:left()
  self:left()
end

return RelativeGps
