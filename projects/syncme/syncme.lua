local _brokerInsertSubscriptionUrlHere = nil
local PROJECTS_CONF = "/projects.conf"

local function createDefaultProjectsConfig()
    return {
        git = {
            url = "https://gitlab.com/abseboco/cc-apps/-/raw/"
        },
        live = {
            url = "default",
        },
        projects = {
            {
                name = "syncme",
                -- allowed: auto,live,git
                type = "auto",
                branch = "main",
                putInto = "/"
            }
        }
    }
end

local function loadProjectsConfig()
    local openfile, err = io.open(PROJECTS_CONF, "r")
    if err ~= nil then
        return nil
    end
    local rawJson = openfile:read("a")
    openfile:close()
    return textutils.unserialize(rawJson)
end

local function saveProjectsConfig(projectsConfig)
    local newFile = io.open(PROJECTS_CONF, "w")
    newFile:write(textutils.serialize(projectsConfig))
    newFile:close()
end

local function createOrLoadProjectsConfig()
    local config = loadProjectsConfig()
    if config == nil then
        print("Creating /projects.conf since it is not present")
        config = createDefaultProjectsConfig()
        saveProjectsConfig(config)
    end
    return config
end

local function printHelp()
    print("Usage: syncme [COMMAND]")
    print("")
    print("Starting syncme without any command is like running 'syncme bg'")
    print("")
    print("Commands:")
    print("  help")
    print("  project <add|remove> <project>")
    print("  update <all|self|projects|deps>")
    print("  startup")
    print("  fg")
    print("  bg")
end

local function installAsStartup()
    local newFile = io.open("/startup.lua", "w")
    newFile:write("shell.run(\"syncme\")")
    print(newFile:close())
end

local function getProjectNames(projectsConfig)
    local resultList = {}
    for _, project in ipairs(projectsConfig.projects) do
        table.insert(resultList, project.name)
    end
    return resultList
end

local function getLiveUrl(config)
    if config.live.url == "default" then
        return _brokerInsertSubscriptionUrlHere
    end
    return config.live.url
end

local function startSyncer(projectsConfig)
    local url = getLiveUrl(projectsConfig)
    print("Starting syncer client", url)

    local ws, err = http.websocket(url)

    if not ws then
        return printError(err)
    end

    local function sendMessage(messageType, payload)
        local message = {
            type = messageType,
            payload = payload
        }
        local asJson = textutils.serializeJSON(message)
        ws.send(asJson)
    end

    local function handleSyncFilesRequest(syncFiles)
        for _, file in ipairs(syncFiles.files) do
            local filepath = syncFiles.destination.."/"..file.name
            print("->", filepath)
            local openfile = io.open(filepath, "w")
            openfile:write(file.content)
            print(openfile:close())
        end
    end

    local function getProjectConfig(projectName)
        for _, projectConfig in ipairs(projectsConfig.projects) do
            if projectConfig.name == projectName then
                return projectConfig
            end
        end
        printError("Unable to find config for '"..projectName.."' in your projects.conf")
    end
    local function handleProjectsUpdatedRequest(projectsUpdatedRequest)
        for _, project in ipairs(projectsUpdatedRequest.projects) do
            print("Project", project.name)
            local projectConfig = getProjectConfig(project.name)
            local targetDirectory = projectConfig.putInto
            for _, file in ipairs(project.files) do
                local absoluteFileTargetPath = targetDirectory..file.path
                print("->", absoluteFileTargetPath)
                local response = http.get(file.url)
                local content = response.readAll()
                response:close()
                local openfile = io.open(absoluteFileTargetPath, "w")
                openfile:write(content)
                openfile:close()
            end
        end
    end

    sendMessage("ComputerBootRequest", {
        id = os.getComputerID(),
        projects = getProjectNames(projectsConfig)
    })

    while true do
        local request = ws.receive()
        if request == nil then
            print("Websocket closed")
            return
        end
        print(request)
        local message = textutils.unserializeJSON(request)
        if message.type == "SyncFilesRequest" then
            handleSyncFilesRequest(message.payload)
        elseif message.type == "ProjectsUpdatedRequest" then
            handleProjectsUpdatedRequest(message.payload)
        else
            printError("Unknown message type", message.type)
        end
    end
end

local function startSyncerLoop(projectsConfig)
    local waitInSeconds = 30
    while true do
        startSyncer(projectsConfig)
        print("Waiting", waitInSeconds, "seconds until next reconnect")
        sleep(waitInSeconds)
    end
end

local function runProjectSubCommand(otherArgs, projectsConfig)
    local subSubCommand = otherArgs[1]
    local projectName = otherArgs[2]

    if subSubCommand == nil then
        printError("missing subcommand: project <add|remove> <project>")
        return
    end
    if projectName == nil then
        printError("missing project: project <add|remove> <project>")
        return
    end
    if subSubCommand == "add" then
        table.insert(projectsConfig.projects, {
            name = projectName,
            type = "auto",
            branch = "main",
            putInto = "/"..projectName.."/"
        })
        saveProjectsConfig(projectsConfig)
    elseif subSubCommand == "remove" then
        local newList = {}
        for _, project in ipairs(projectsConfig.projects) do
            if projectName ~= project.name then
                table.insert(newList, project)
            end
        end
        projectsConfig.projects = newList
        saveProjectsConfig(projectsConfig)
    else
        printError("invalid subcommand: project <add|remove> <project>")
        printError("subcommand:", subSubCommand)
    end
end

local function runUpdateSubCommand(otherArgs)
    local subSubCommand = otherArgs[1]
    if subSubCommand == "all" then
        print("TODO update all")
    elseif subSubCommand == "self" then
        print("TODO update self")
    elseif subSubCommand == "projects" then
        print("TODO update projects")
    elseif subSubCommand == "deps" then
        print("TODO update deps")
    else
        print("SubSubCommand unknown:", subSubCommand)
    end
end

local function main()
    local subCommand = arg[1]
    local otherArgs = {}
    local sizeOfOtherArgs = table.getn(arg)-1
    for i=2,1+sizeOfOtherArgs do
        otherArgs[i-1] = arg[i]
    end

    local projectsConfig = createOrLoadProjectsConfig()

    if subCommand == "help" then
        printHelp()
    elseif subCommand == "project" then
        runProjectSubCommand(otherArgs, projectsConfig)
    elseif subCommand == "update" then
        runUpdateSubCommand(otherArgs)
    elseif subCommand == "startup" then
        installAsStartup()
    elseif subCommand == "fg" then
        startSyncerLoop(projectsConfig)
    elseif subCommand == "bg" or subCommand == nil then
        print("Running SyncMe as background task")
        local id = shell.openTab("syncme fg")
        multishell.setTitle(id, "SyncMe")
    else
        printError("Unknown command '" .. subCommand .. "'")
    end
end

main()