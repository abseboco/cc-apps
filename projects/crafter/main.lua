print("Start crafter")

local sendChannel = 431
local receiveChannel = 432
local modem = nil

local function sendMessage(message)
    modem.transmit(sendChannel, receiveChannel, message)
end

local function grabItemFromInput()

end

local function craftItem(payload)
    return turtle.craft(payload.count)
end

local function putItemsToInput()

end

local function handleCraftNow(message)
    grabItemFromInput()
    local success, errorMessage = craftItem(message)
    putItemsToInput()
    sendMessage({
        type = "FinishedCrafting",
        success = success,
        errorMessage = errorMessage
    })
end

local function handleIsCrafterPresent()
    sendMessage({
        type = "CrafterIsHere"
    })
end

local function receiveAndExecute()
    local _, _, channel, replyChannel, message = os.pullEvent("modem_message")
    if receiveChannel ~= 43 then
        return
    end
    print("Channels", channel, replyChannel)
    print("Received a reply: " .. tostring(message))
    if message.type == "IsCrafterPresent" then
        handleIsCrafterPresent()
    elseif message.type == "CraftNow" then
        handleCraftNow(message)
    end
end

local function main()
    modem = peripheral.find("modem") or error("No modem attached", 0)
    modem.open(receiveChannel)

    while true do
        receiveAndExecute()
    end
end

main()

