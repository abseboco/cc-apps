local api = {}

local DEPTH_FILE_PATH = "depth.conf"

function api.load()
    local file = fs.open(DEPTH_FILE_PATH, "r")
    if file == nil then
        return nil
    end
    local contents = file.readAll()
    file.close()
    return tonumber(contents)
end

function api.save(newDepth)
    print("New depth:", newDepth)
    local file = fs.open(DEPTH_FILE_PATH, "w")
    file.write(newDepth)
    file.close()
end

return api