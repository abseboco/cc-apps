local safeDigger = require("safeDigger")
local relativegps = require("libturtle.relativegps")
local START_POSITION_FILE_PATH = "start_position.conf"

local function getCurrentPositionAndRotation()
    local x1, y1, z1 = gps.locate()
    if x1 == nil then
        return nil, "Unable to determinte current position: No response from GPS"
    end
    safeDigger.safeDig()
    if not turtle.forward() then
        return nil, "Unable to determinte current rotation: Need to move forward once"
    end
    local x2, _, z2 = gps.locate()
    turtle.back()

    local facing
    if x1 > x2 then
        facing = "x-"
    elseif x1 < x2 then
        facing = "x+"
    else
        if z1 > z2 then
            facing = "z-"
        elseif z1 < z2 then
            facing = "z+"
        else
            return nil, "Unable to determinte current rotation: Turtle moved but gps position did not change"
        end
    end
    return {
        x = x1,
        y = y1,
        z = z1,
        facing = facing
    }
end

local function loadStartPositionAndRotation()
    local file = fs.open(START_POSITION_FILE_PATH, "r")
    if file == nil then
        return nil
    end
    local contents = file.readAll()
    file.close()

    local data = textutils.unserialize(contents)
    if data == nil then
        error("Config file is invalid: "..START_POSITION_FILE_PATH)
    end
    return data
end

local function savePositionAndRotation(posRot)
    print("Saving current position as start position")
    local str = textutils.serialise(posRot)

    local file = fs.open(START_POSITION_FILE_PATH, "w")
    file.write(str)
    file.close()
end

local function goToStartPositionOrSaveCurrentPositionAsStartPosition()
    local startPosRot = loadStartPositionAndRotation()
    local currentPosRot, errorMessage = getCurrentPositionAndRotation()
    if not currentPosRot then
        return false, errorMessage
    end
    if startPosRot then
        print("Start position is already known")
        local rgps = relativegps:new({
            x = currentPosRot.x,
            y = currentPosRot.y,
            z = currentPosRot.z,
            facing = currentPosRot.facing,
            trackOperations = false
        })
        rgps:goTo(startPosRot.x, startPosRot.y, startPosRot.z)
        rgps:turnTo(startPosRot.facing)
    else
        savePositionAndRotation(currentPosRot)
    end
    return true, nil
end

return {
    goToStartPositionOrSaveCurrentPositionAsStartPosition = goToStartPositionOrSaveCurrentPositionAsStartPosition
}