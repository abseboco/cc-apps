local BLOCK_BLACKLIST = {"minecraft:obsidian", "minecraft:chest", "minecraft:barrel"}
local ENCOUNTERED_BLACKLISTED_BLOCK_MESSAGE = "Encountered blacklisted block"
local BLACKLISTED_BLOCK_ERROR_MESSAGE = "Block is not allowed to break"

local function isBlockBlacklisted(block)
    for _, blacklisted_block_name in ipairs(BLOCK_BLACKLIST) do
        if block.name == blacklisted_block_name then
            return true
        end
    end
    return false
end

local function safeDig()
    local has_block, block = turtle.inspect()
    if has_block and isBlockBlacklisted(block) then
        printError(ENCOUNTERED_BLACKLISTED_BLOCK_MESSAGE)
        return false, BLACKLISTED_BLOCK_ERROR_MESSAGE
    end
    return turtle.dig()
end

local function safeDigUp()
    local has_block, block = turtle.inspectUp()
    if has_block and isBlockBlacklisted(block) then
        printError(ENCOUNTERED_BLACKLISTED_BLOCK_MESSAGE)
        return false, BLACKLISTED_BLOCK_ERROR_MESSAGE
    end
    return turtle.digUp()
end

local function safeDigDown()
    local has_block, block = turtle.inspectDown()
    if has_block and isBlockBlacklisted(block) then
        printError(ENCOUNTERED_BLACKLISTED_BLOCK_MESSAGE)
        return false, BLACKLISTED_BLOCK_ERROR_MESSAGE
    end
    return turtle.digDown()
end

return {
    safeDig = safeDig,
    safeDigUp = safeDigUp,
    safeDigDown = safeDigDown
}