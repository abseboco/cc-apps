# CC Apps

Programs for CC:Tweaked

## Using run-dev to install a project

Example installation of `sorter`:

 * `mkdir sorter` (optional)
 * `cd sorter` (optional)
 * `wget https://gitlab.com/abseboco/cc-apps/-/raw/main/dev.lua`
 * `set run-dev.project sorter` (optional)
 * `set run-dev.run-script sorter` (optional)
 * `set run-dev.branch main` (optional)

Update and run:

 * `dev run`

Update:

 * `dev update app`

## Sorter

Use a turtle to sort items.

[README](sorter/README.md)

# Development

* Gitpod dev environment is alredy set up
* Run `luacheck **/*.lua` to check linting errors