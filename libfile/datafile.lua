local api = {}

--lib to serialize and deserialize lua objects into and from files

function api.writeFile(absoluteFilenpath, data)
  local s = textutils.serialize(data)
  local handle = io.open(absoluteFilenpath, 'w')
  handle:write(s)
  handle:flush()
  handle:close()
end

function api.loadFile(absoluteFilenpath)
  local handle, _ = io.open(absoluteFilenpath, 'r')
  if handle == nil then
    return nil
  end
  local data = handle:read('a')
  handle:close()
  return textutils.unserialize(data)
end

return api
