-- libs
local pretty = require('cc.pretty')

local RelativeGps = require('libturtle.relativegps')

local inventoryIndex = require('inventory-indexer')
local scanSurroundings = require('scan-surroundings')
local shared = require('shared')
local collectItems = require('collect-items')

-- args & config

local SELF_HOSTNAME = 'sorter-turtle'
local TARGET_HOSTNAME = 'sorter-monitor'
local PROTOCOL = 'absebo-sorter'

local modem = peripheral.find('modem')
if modem then
  local modemSide = peripheral.getName(modem)
  rednet.open(modemSide)
  rednet.host(PROTOCOL, SELF_HOSTNAME)
end

local args = {...}

local function loadConfig()
  local config = {}
  local function useDefaultIfNotPresent(key, default)
    local value = settings.get(key)
    if value == nil then
      return default
    end
    return value
  end
  config.wait_after_signal = useDefaultIfNotPresent('sorter.wait_after_signal', 5)
  config.fuel_level_required = useDefaultIfNotPresent('sorter.fuel_level_required', 500)
  return config
end

local config = loadConfig()

-- app

local function sendMessage(message)
  local id = rednet.lookup(PROTOCOL, TARGET_HOSTNAME)
  if not id then
    print('Target host not available')
    return
  end
  print('Send message:', pretty.pretty(message))
  rednet.send(id, message, PROTOCOL)
end

local function handleMessage(message)
  print('Received message:', pretty.pretty(message))
  if message.type == 'GetIndexRequest' then
    sendMessage({
      type = 'GetIndexResponse',
      data = inventoryIndex.getIndexData()
    })
  elseif message.type == 'GetItemsRequest' then
    collectItems.findAndCollectItems(
      inventoryIndex.getIndexData(),
      config,
      message.items
    )
  end
end

local function putAllItemsBack()
  print('Put all other items back')
  for i=1,16 do
    if turtle.getItemCount(i) > 0 then
      turtle.select(i)
      if not turtle.drop() then
        error('Can not put items back')
      end
    end
  end
  turtle.select(1)
end

local function checkFuel()
  if turtle.getFuelLevel() >= config.fuel_level_required then
    print('Fuel: '..turtle.getFuelLevel())
    return
  end
  sendMessage('Refuel:'..turtle.getFuelLevel())
  putAllItemsBack()

  while turtle.getFuelLevel() < config.fuel_level_required do
    print('Checking fuel (' .. turtle.getFuelLevel() .. ') to reach ' .. config.fuel_level_required)
    turtle.suckDown(1)
    if not turtle.refuel() then
       error('Unable to refuel')
    end
  end
  print('Refuel completed')
  putAllItemsBack()
end

local function suckAll()
  sendMessage('Collect')
  local suckedOneTime = false
  while turtle.suck() do
    suckedOneTime = true
  end
  print('Sucked all')
  return suckedOneTime
end

local function itemInfoToName(itemInfo)
  return shared.longToShortItemName(itemInfo.longItemName)..'('..table.concat(itemInfo.slots, ',')..')'
end

local function sortItemsInSlots(rgps, itemInfo)
  --index of item
  local itemName = itemInfoToName(itemInfo)
  sendMessage('Sort '..itemName..' '..itemInfo.costScore)
  if not itemInfo.indexFound then
    print(itemName..' was not found in index')
    return
  end
  print(itemName..' was found in index')
  for _, foundIndexItem in ipairs(itemInfo.storagePositions) do
    shared.goToChest(rgps, config, foundIndexItem)
    local dropped = true
    for _, slot in ipairs(itemInfo.slots) do
      turtle.select(slot)
      if not turtle.drop() then
        dropped = false
      end
    end

    if dropped then
      print('Could sort item '..itemName)
      return
    end
  end
  print('Could not sort item '..itemName)
end

local function calcCostScore(foundIndexList)
  if not foundIndexList then
    return 0
  end
  local firstStoragePosition = foundIndexList[1]
  if not firstStoragePosition then
    error('Empty index list')
  end
  return firstStoragePosition.x + firstStoragePosition.y + (firstStoragePosition.z * 10)
end

--example return value: {
--  'minecraft:emerald' = {
--     indexFound = true,
--     slots = {1,4},
--     storagePositions = vector-array
--     costScore = 146 --lower is better
--  }
--}
--that means item emerald is present in slot 1 and 4
local function groupSlotsWithEqualItems()
  local result = {}
  for slotNumber=1,16 do
    local detail = turtle.getItemDetail(slotNumber)
    if detail then
      local info = result[detail.name]
      if not info then
        local foundIndexList = inventoryIndex.getIndexData()[detail.name]
        info = {
          indexFound = not not foundIndexList,
          storagePositions = foundIndexList,
          slots = {},
          costScore = calcCostScore(foundIndexList)
        }
        result[detail.name] = info
      end
      table.insert(info.slots, slotNumber)
    end
  end
  return result
end

local function sortByScore(grouped)
  local arr = {}
  for longItemName, itemInfo in pairs(grouped) do
    itemInfo.longItemName = longItemName
    table.insert(arr, itemInfo)
  end
  table.sort(arr, function (k1, k2) return k1.costScore < k2.costScore end )
  return arr
end

local function doSortCycle()
  print('Starting to sort')
  local grouped = groupSlotsWithEqualItems()
  local rgps = RelativeGps:new({
    x = 0,
    y = 0,
    z = 0,
    facing = 'x-'
  })
  local sorted = sortByScore(grouped)
  for _, itemInfo in ipairs(sorted) do
    print(itemInfo.longItemName, itemInfo.indexFound, itemInfo.costScore)
  end
  for _, itemInfo in ipairs(sorted) do
    sortItemsInSlots(rgps, itemInfo)
  end
  if rgps.z ~= 0 then
    shared.traverseHole(rgps, config, 0)
  end
  shared.returnToInputChest(rgps)
  --everything else could not be sorted
  turtle.turnRight()
  putAllItemsBack()
  turtle.turnLeft()
end

local function waitForEvents()
  sendMessage('Wait')
  while true do
    local eventData = {os.pullEvent()}
    local eventType = eventData[1]
    if eventType == 'redstone' then
      sleep(config.wait_after_signal)
      return
    elseif eventType == 'rednet_message' then
      local protocol = eventData[4]
      if protocol == PROTOCOL then
        handleMessage(eventData[3])
      end
    end
  end
end

local function startSortLoop()
  print('Staring sort loop')
  while true do
    checkFuel()
    local sucked = suckAll()
    if sucked then
      sendMessage('Sort')
      doSortCycle()
    else
      print('No items to sort')
      waitForEvents()
    end
  end
end

local function main()
  print('Starting...')
  checkFuel()
  sendMessage('Starting')

  local subcommand = args[1]
  if subcommand == nil then
    sendMessage('Bounds')
    config.bounds = scanSurroundings.checkBoundIfNecessary()
    sendMessage('Index')
    inventoryIndex.indexInventoriesIfNecessary(config)
    startSortLoop()
    return
  end

  if subcommand == 'all' then
    sendMessage('Force all: scan')
    config.bounds = scanSurroundings.forceCheckBounds()
    sendMessage('Force all: index')
    inventoryIndex.forceIndexInventories(config)
    startSortLoop()
  elseif subcommand == 'bounds' then
    sendMessage('Force bounds')
    config.bounds = scanSurroundings.forceCheckBounds()
  elseif subcommand == 'index' then
    sendMessage('Force index')
    config.bounds = scanSurroundings.checkBoundIfNecessary()
    inventoryIndex.forceIndexInventories(config)
  end

end

main()
