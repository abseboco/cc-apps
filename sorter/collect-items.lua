local RelativeGps = require('libturtle.relativegps')
local shared = require('shared')

local api = {}

local function findAndCollectItem(rgps, indexData, config, item)
  local foundItemList = indexData[item.name]
  if foundItemList == nil then
    print('Item not found in index:', item.name)
    return
  end
  for _, foundItem in ipairs(foundItemList) do
    shared.goToChest(rgps, config, foundItem)
    turtle.suck(item.amount)
  end
end

--listOfItems = {{name = 'minecraft:gold_ingot', amount = 3}, ...}
function api.findAndCollectItems(indexData, config, listOfItems)
  local rgps = RelativeGps:new({
    facing = 'x-'
  })
  for _, item in ipairs(listOfItems) do
    print('item:', item, item.name)
    findAndCollectItem(rgps, indexData, config, item)
  end
  shared.returnToInputChest(rgps)
end

return api
