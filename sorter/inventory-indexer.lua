local turtleplus = require('libturtle.turtleplus')
local datafile = require('libfile.datafile')

local shared = require('shared')

local INDEX_FILE = 'index'

local api = {}
local indexData = nil

local function writeIndex(data)
  datafile.writeFile(INDEX_FILE, data)
end

local function loadIndex()
  return datafile.loadFile(INDEX_FILE)
end

local function contains(array, item)
  for _, it in ipairs(array) do
    if it == item then
      return true
    end
  end
  return false
end

local function addChestToIndex(data, chestSide, x, y, z)
  local chest = peripheral.find('minecraft:chest', function(side) return side == chestSide end)
  if not chest then
    return false
  end
  -- list of items with name and count
  local items = chest.list()
  for _, item in pairs(items) do
    local itemIndexArray = data[item.name]
    if not itemIndexArray then
      itemIndexArray = {}
      data[item.name] = itemIndexArray
    end
    local pos = vector.new(x, y, z)
    if not contains(itemIndexArray, pos) then
      print('[Index] '..item.name..' at '..pos.x..'|'..pos.y..'|'..pos.z)
      table.insert(itemIndexArray, pos)
    end
  end
  return true
end

local function indexPane(config, data, chestSide, z)
  local chestIndexed = 0
  for y=0,config.bounds.yLength do
    if y ~= 0 then
      turtle.up()
    end
    for x=0,config.bounds.xLength do
      if x ~= 0 then
        turtle.back()
      end
      if addChestToIndex(data, chestSide, x, y, z) then
        chestIndexed = chestIndexed + 1
      end
    end
    turtleplus.forward(config.bounds.xLength)
  end
  turtleplus.down(config.bounds.yLength)
  return chestIndexed
end

function api.getIndexData()
  if indexData == nil then
    error('Run index before accessing')
  end
  return indexData
end

function api.forceIndexInventories(config)
  print('Indexing')
  turtle.equipRight()
  local data = {}
  -- index origin side
  local chestIndexed = indexPane(config, data, 'left', 0)
  -- index hole side
  if config.bounds.holeX then
    shared.travelFromOriginToHoleOrigin(config)
    chestIndexed = chestIndexed + indexPane(config, data, 'right', config.bounds.holeWidth)
    shared.travelFromHoleOriginToOrigin(config)
  end
  -- finished index
  writeIndex(data)
  indexData = data
  turtle.equipRight()
  print('Index completed with '..chestIndexed..' chests indexed')
end

function api.indexInventoriesIfNecessary(config)
  indexData = loadIndex()
  if indexData == nil then
    api.forceIndexInventories(config)
    return
  end
  print('Used cache to get index')
end

return api
