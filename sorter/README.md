
# Install

 * `mkdir sorter` (optional)
 * `cd sorter` (optional)
 * `wget run https://gitlab.com/abseboco/cc-apps/-/raw/main/sorter/install.lua`

# Usage

 * Place a chest. This will be the fuel chest.
 * Place an advanced turtle on top of the fuel chest.
 * Place a chest in front of the turtle. This will be the item-input chest.
 * Place more chests so it looks like this from top
   * T = Turtle
   * C = Chest
   * I = Input chest
```
-II-
--T-
CC--
CC--
CC--
CC--
CC--
CC--
and more
```
 


# Development

Install (1x):

 * `mkdir sorter` (optional)
 * `cd sorter` (optional)
 * `set run-dev.project sorter`
 * `set run-dev.branch main`
 * `wget https://gitlab.com/abseboco/cc-apps/-/raw/main/run-dev.lua`

Run:

 * `run-dev`
