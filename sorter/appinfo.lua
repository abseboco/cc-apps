return {
    files = {
        'sorter.lua',
        'inventory-indexer.lua',
        'scan-surroundings.lua',
        'shared.lua',
        'collect-items.lua'
    },
    libs = {
        'libturtle',
        'libfile'
    }
}
