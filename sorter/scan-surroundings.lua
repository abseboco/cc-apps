local turtleplus = require('libturtle.turtleplus')
local datafile = require('libfile.datafile')

local BOUNDS_FILE = 'bounds'

local api = {}

local function writeBoundsFile(data)
  datafile.writeFile(BOUNDS_FILE, data)
end

local function loadBoundsFile()
  return datafile.loadFile(BOUNDS_FILE)
end

local function checkBounds()
  --we expect to be at the origin position
  --yLength
  local yLength = turtleplus.upUntilHit()
  turtleplus.down(yLength)
  --xLength
  local xLength = turtleplus.backUntilHit()
  --hole
  local holeX = nil
  local holeDepth = nil
  local holeWidth = nil
  for x=xLength,1,-1 do
    if not turtle.detectDown() then
      --there is a hole
      holeX = x
      holeDepth = turtleplus.downUntilHit()
      turtle.turnLeft()
      holeWidth = turtleplus.forwardUntilHit()
      turtleplus.back(holeWidth)
      turtle.turnRight()
      turtleplus.up(holeDepth)
    end
    turtle.forward()
   end
  --x is not really an x coordinate.
  -- * it is relative like y to the origin coordinate
  -- * it could be a z coordinate. That depends on how you rotate the turtle in the origin position
  return {
    xLength = xLength,
    yLength = yLength,
    holeX = holeX,
    holeDepth = holeDepth,
    holeWidth = holeWidth
  }
end

function api.forceCheckBounds()
  local bounds = checkBounds()
  writeBoundsFile(bounds)
  return bounds
end

function api.checkBoundIfNecessary()
  local bounds = loadBoundsFile()
  if bounds then
    print('Used cache to get bounds')
    return bounds
  end
  return api.forceCheckBounds()
end

return api
