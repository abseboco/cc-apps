local turtleplus = require('libturtle.turtleplus')

local api = {}

local function switchSide(config, startAtHoleSide)
  if not config.bounds.holeX then
    error('Can not switch if hole is not defined')
  end
  turtleplus.back(config.bounds.holeX)
  turtleplus.down(config.bounds.holeDepth)
  if startAtHoleSide then
    turtle.turnRight()
  else
    turtle.turnLeft()
  end
  turtleplus.forward(config.bounds.holeWidth)
  if startAtHoleSide then
    turtle.turnLeft()
  else
    turtle.turnRight()
  end
  turtleplus.up(config.bounds.holeDepth)
  turtleplus.forward(config.bounds.holeX)
end

function api.travelFromOriginToHoleOrigin(config)
  print('Switching side to hole side')
  switchSide(config, false)
end

function api.travelFromHoleOriginToOrigin(config)
  print('Switching side to origin side')
  switchSide(config, true)
end

function api.traverseHole(rgps, config, z)
  print('Traverse hole ' .. z)
  rgps:goTo(config.bounds.holeX, 0, nil)
  rgps:down(config.bounds.holeDepth)
  rgps:goTo(nil, nil, z)
  rgps:up(config.bounds.holeDepth)
end

function api.longToShortItemName(longItemName)
  return string.gsub(longItemName, 'minecraft:','')
end

function api.goToChest(rgps, config, chestVector)
  local side
  if chestVector.z > 0 then
    side = 'z-'
  else
    side = 'z+'
  end
  -- maybe traverse hole
  if chestVector.z ~= rgps.z then
    api.traverseHole(rgps, config, chestVector.z)
  end
  rgps:goTo(chestVector.x, chestVector.y, nil)
  --put into chest
  rgps:turnTo(side)
  rgps:printPosition()
end

function api.returnToInputChest(rgps)
  rgps:goTo(0, 0, 0)
  rgps:turnTo('x-')
end

return api
