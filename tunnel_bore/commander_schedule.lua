print("Init commander_schedule")

--"packages"
local mes = require("messaging")
local tops = require("turtle_ops")
local fc = require("fuel_check")

--varibales
local channel = 43
local mute = true

--init
function init_slime(channel, mute)
    mes.send_message(channel, "Init Slime", mute)
    target = "Slime Ready"
    mes.receive_message(channel, target, mute)
end

function init(channel, mute)
    mes.send_message(channel, "Piston Start", mute)

    target = "Piston Ready"
    mes.receive_message(channel, target, mute)
    init_slime(channel, mute)
end

--fuel functions
function fuel_dialog(channel, mute)
    mes.send_message(channel, "Send fuel_bool", mute)

    dtype = "boolean"
    fuel_bool = mes.receive_data(channel, dtype, mute)
    mes.send_message(channel, "Received fuel_bool", mute)
    return fuel_bool
end

function fuel_overview(channel, mute)
    mes.send_message(channel, "Collect Fuel Levels", mute)

    target = "Collected Fuel Levels"
    mes.receive_message(channel, target, mute)
    
    mes.send_message(channel, "Send Fuel Levels", mute)
    dtype = "table"
    fuel_list = mes.receive_data(channel, dtype, mute)

    fuel_level = turtle.getFuelLevel()
    print("Commander Fuel Level:", fuel_level)
    print("Piston Fuel Level:", fuel_list[1])
    print("Slime Fuel Level:", fuel_list[2])
end

--commands
function eval_continue()
    not_powered = tops.not_powered()
    enough_fuel = turtle.getFuelLevel() > 0
    
    return not_powered and enough_fuel
end

function send_command(channel, mute)
    fuel_bool = fuel_dialog(channel, mute)

    target = "Send Command"
    mes.receive_message(channel, target, mute)

    continue = eval_continue()
    continue = continue and fuel_bool
    if continue then
        mes.send_message(channel, "Go", mute)
    else
        mes.send_message(channel, "Stop", mute)
    end

    target = "Received Command"
    mes.receive_message(channel, target, mute)
    return continue
end

function cascade_command(channel, mute)
    mes.send_message(channel, "Cascade Command", mute)
    target = "Cascading"
    mes.receive_message(channel, target, mute)

    target = "Cascaded Successfully"
    mes.receive_message(channel, target, mute)
end

--mining steps
function step_dialog(channel, mute)
    mes.send_message(channel, "Execute Steps", mute)

    target = "Steps Executed"
    mes.receive_message(channel, target, mute)
end

--main loop
function main(channel, mute)
    init(channel, mute)

    continue = send_command(channel, mute)
    cascade_command(channel, mute)
    while continue do
        step_dialog(channel, mute)

        continue = send_command(channel, continue, mute)
        cascade_command(channel, mute)
        tops.dig_forward()

        fuel_overview(channel, mute)
    end
end

main(channel, mute)
