local api = {}
--Fuel cost per operation of one tunnel
--bore movement

function calculate_cost(num_runs)
local fuel_cost = 1
    return num_runs * fuel_cost
end

function api.check_fuel(num_runs)
    fuel_level = turtle.getFuelLevel()
    cost = calculate_cost(num_runs)
    return fuel_level >= cost
end

return api