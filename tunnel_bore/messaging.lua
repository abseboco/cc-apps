local api = {}

function api.receive_message(channel, target, mute)
    local msg = nil
    if not mute then
        print("Checking for word", target)
    end
    modem = peripheral.find("modem")
    modem.open(channel)
    while msg ~= target do
        _, _, _, _, msg, _ = os.pullEvent("modem_message")
        if not mute then
            print("Received message:", msg)
        end
    end
    modem.close(channel)
end

function api.send_message(channel, msg, mute)
    modem = peripheral.find("modem")
    if not mute then
        print("Sending message:", msg)
    end
    modem.transmit(channel, 1, msg)
end

function api.receive_data(channel, dtype, mute)
    local msg = nil
    modem = peripheral.find("modem")
    modem.open(channel)
    if not mute then
        print("Receiving data")
    end
    while type(msg) ~= dtype do
        _, _, _, _, msg, _ = os.pullEvent("modem_message")
        if not mute then
            print("Received data:", msg)
        end
    end
    modem.close(channel)
    
    return msg
end   

return api
