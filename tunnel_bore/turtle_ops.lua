local api = {}
 
function api.place_piston()
    turtle.place()
end
 
function api.pulse()
    redstone.setAnalogOutput("front", 1)
    sleep(1)
    redstone.setAnalogOutput("front", 0)
end
 
function api.collect_piston()
    turtle.dig()
end
 
function api.piston_move_next()
    turtle.forward()
end
 
-- Slime
function api.break_slime()
    turtle.dig()
end
 
function api.place_slime()
    turtle.place()
end
 
function api.slime_move_next()
    turtle.turnRight()
    turtle.forward()
    turtle.turnLeft()
end

--Commander
function api.dig_forward()
    turtle.dig()
    turtle.forward()
    turtle.dig()
    turtle.digUp()
    turtle.digDown()
end

function api.not_powered()
    n = redstone.getAnalogInput("left")
    n = n + redstone.getAnalogInput("right")
    n = n + redstone.getAnalogInput("front")
    n = n + redstone.getAnalogInput("back")
    n = n + redstone.getAnalogInput("top")
    n = n + redstone.getAnalogInput("bottom")
    
    return n == 0
end

return api
