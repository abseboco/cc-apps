return {
    files = {
        'commander_schedule.lua',
        'fuel_check.lua',
        'messaging.lua',
        'piston_schedule.lua',
        'slime_schedule.lua',
        'turtle_ops.lua'
    }
}
