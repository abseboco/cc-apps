print("Init piston_schedule")

--"packages"
local mes = require("messaging")
local tops = require("turtle_ops")
local fc = require("fuel_check")

--Variables
local commander_channel = 43
local channel = 42
local num_iter = 1
local pause = 6
local mute = true

--init functions
function init_slime_dialog(channel, num_iter, mute)
    mes.send_message(channel, "Start Slime", mute)

    target = "Send num_iter"
    mes.receive_message(channel, target, mute)
    mes.send_message(channel, num_iter, mute)

    target = "Received Number"
    mes.receive_message(channel, target, mute)
end

function init_slime(commander, channel, num_iter, mute)
    target = "Init Slime"
    mes.receive_message(commander, target, mute)
    
    init_slime_dialog(channel, num_iter, mute)
    mes.send_message(commander, "Slime Ready", mute)
end

function init(commander, channel, num_iter, mute)
    target = "Piston Start"
    mes.receive_message(commander, target, mute)
    mes.send_message(commander, "Piston Ready", mute)
    init_slime(commander, channel, num_iter, mute)
end

--fuel functions
function communicate_fuel(channel, mute)
    fuel_bool = fc.check_fuel(num_iter)
    mes.send_message(channel, fuel_bool, mute)

    target = "Received fuel_bool"
    mes.receive_message(channel, target, mute)
    mes.send_message(channel, "Ready To Receive fuel_bool", mute)

    dtype = "boolean"
    fuel_bool_2 = mes.receive_data(channel, dtype, mute)
    mes.send_message(channel, "Received fuel_bool", mute)

    target = "Waiting For Command"
    mes.receive_message(channel, target, mute)
    return fuel_bool and fuel_bool_2
end

function fuel_slime_dialog(channel, mute)
    mes.send_message(channel, "Init Fuel Dialog", mute)

    target = "Send fuel_bool"
    mes.receive_message(channel, target, mute)
    fuel_bool = communicate_fuel(channel, mute)

    return fuel_bool
end

function fuel_dialog(commander, channel, mute)
    target = "Send fuel_bool"
    mes.receive_message(commander, target, mute)

    fuel_bool = fuel_slime_dialog(channel, mute)
    mes.send_message(commander, fuel_bool, mute)

    target = "Received fuel_bool"
    mes.receive_message(commander, target, mute)
end

function fuel_overview(commander, channel, mute)
    target = "Collect Fuel Levels"
    mes.receive_message(commander, target, mute)
    mes.send_message(channel, "Send Fuel Level", mute)

    dtype = "number"
    fuel_level_2 = mes.receive_data(channel, dtype, mute)
    mes.send_message(channel, "Received Fuel Level", mute)

    fuel_level_1 = turtle.getFuelLevel()
    fuel_list = {fuel_level_1, fuel_level_2}
    mes.send_message(commander, "Collected Fuel Levels", mute)

    target = "Send Fuel Levels"
    mes.receive_message(commander, target, mute)
    mes.send_message(commander, fuel_list, mute)
end

--commands
function receive_command(commander, channel, mute)
    fuel_dialog(commander, channel, mute)
    mes.send_message(commander, "Send Command", mute)

    dtype = "string"
    msg = mes.receive_data(commander, dtype, mute)
    mes.send_message(commander, "Received Command", mute)
    return msg
end

function send_command(commander, channel, msg, mute)
    target = "Cascade Command"
    mes.receive_message(commander, target, mute)
    mes.send_message(commander, "Cascading", mute)
    mes.send_message(channel, "Command Ready", mute)

    target = "Send Command"
    mes.receive_message(channel, target, mute)
    mes.send_message(channel, msg, mute)

    target = "Received Command"
    mes.receive_message(channel, target, mute)
    mes.send_message(commander, "Cascaded Successfully", mute)
end

--mining steps
function mining_step(channel, mute)
    mes.send_message(channel, "Break Slime", mute)

    target = "Place Piston"
    mes.receive_message(channel, target, mute)
    tops.place_piston()
    mes.send_message(channel, "Place Slime", mute)

    target = "Redstone Pulse"
    mes.receive_message(channel, target, mute)
    tops.pulse()
    tops.collect_piston()
    mes.send_message(channel, "Slime Move", mute)

    target = "Piston Move"
    mes.receive_message(channel, target, mute)
    tops.piston_move_next()
end

function do_steps(channel, num_runs, pause, mute)
    for i=1,num_runs do
        mining_step(channel, mute)
        sleep(pause)
    end
    mes.send_message(channel, "End Of Steps", mute)
end

function step_dialog(commander, channel, mute)
    target = "Execute Steps"
    mes.receive_message(commander, target, mute)
    mes.send_message(channel, "Execute Steps", mute)

    target = "Ready For Steps"
    mes.receive_message(channel, target, mute)
    do_steps(channel, num_iter, pause, mute)

    target = "Steps Executed"
    mes.receive_message(channel, target, mute)
    mes.send_message(commander, "Steps Executed", mute)
end

--main loop
function main_loop(commander, channel, num_iter, pause, mute)
    init(commander, channel, num_iter, mute)

    msg = receive_command(commander, channel, mute)
    send_command(commander, channel, msg, mute)

    while msg == "Go" do
        step_dialog(commander, channel, mute)
       
        msg = receive_command(commander, channel, mute)
        send_command(commander, channel, msg, mute)

        fuel_overview(commander, channel, mute)
    end
end

main_loop(commander_channel, channel,
         num_iter, pause, mute)
