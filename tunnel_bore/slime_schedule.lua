print("Init slime_schedule")

--"packages"
local mes = require("messaging")
local tops = require("turtle_ops")
local fc = require("fuel_check")

--Variables
local channel = 42
local mute = true

--init
function init(channel, mute)
    target = "Start Slime"
    mes.receive_message(channel, "Start Slime", mute)
    mes.send_message(channel, "Send num_iter", mute)

    dtype = "number"
    num_iter = mes.receive_data(channel, dtype, mute)
    mes.send_message(channel, "Received Number", mute)
    return num_iter
end

--fuel functions
function communicate_fuel(channel, num_iter, mute)
    mes.send_message(channel, "Send fuel_bool", mute)
    dtype = "boolean"
    fuel_bool_2 = mes.receive_data(channel, dtype, mute)
    mes.send_message(channel, "Received fuel_bool", mute)
    
    target = "Ready To Receive fuel_bool"
    mes.receive_message(channel, target, mute)
    fuel_bool = fc.check_fuel(num_iter)
    mes.send_message(channel, fuel_bool, mute)

    target = "Received fuel_bool"
    mes.receive_message(channel, target, mute)
    mes.send_message(channel, "Waiting For Command", mute)
    
    return fuel_bool and fuel_bool_2
end

function fuel_dialog(channel, num_iter, mute)
    target = "Init Fuel Dialog"
    mes.receive_message(channel, target, mute)

    fuel_bool = communicate_fuel(channel, num_iter, mute)
    return fuel_bool
end

function fuel_overview(channel, mute)
    target = "Send Fuel Level"
    mes.receive_message(channel, target, mute)
    fuel_level = turtle.getFuelLevel()
    mes.send_message(channel, fuel_level, mute)

    target = "Received Fuel Level"
    mes.receive_message(channel, target, mute)
end

--commands
function receive_command(channel, num_iter, mute)
    fuel_bool = fuel_dialog(channel, num_iter, mute)

    target = "Command Ready"
    mes.receive_message(channel, target, mute)
    mes.send_message(channel, "Send Command", mute)

    dtype = "string"
    msg = mes.receive_data(channel, dtype, mute)
    mes.send_message(channel, "Received Command", mute)
    return msg
end

--mining steps
function mining_step(channel, mute)
    target = "Break Slime"
    mes.receive_message(channel, target, mute)
    tops.break_slime()
    mes.send_message(channel, "Place Piston", mute)
    
    target = "Place Slime"
    mes.receive_message(channel, target, mute)
    tops.place_slime()
    mes.send_message(channel, "Redstone Pulse", mute)
    
    target = "Slime Move"
    mes.receive_message(channel, target, mute)
    tops.slime_move_next()
    mes.send_message(channel, "Piston Move", mute)
end

function do_steps(channel, num_iter, mute)
    for i=1,num_iter do
        mining_step(channel, mute) 
    end 
    target = "End Of Steps"
    mes.receive_message(channel, target, mute)
end

function step_dialog(channel, num_iter, mute)
    target = "Execute Steps"
    mes.receive_message(channel, target, mute)
    mes.send_message(channel, "Ready For Steps", mute)

    do_steps(channel, num_iter, mute)
    mes.send_message(channel, "Steps Executed", mute)
end

--main loop
function main_loop(channel, mute)
    num_iter = init(channel, mute)

    msg = receive_command(channel, num_iter, mute)

    while msg == "Go" do
        step_dialog(channel, num_iter, mute)

        msg = receive_command(channel, num_iter, mute)

        fuel_overview(channel, mute)
    end
end

main_loop(channel, mute)
