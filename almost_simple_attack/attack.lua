print("Init")

local egg_slot = 1
local attack_slot = 8

function kill()
    turtle.select(attack_slot)
    attack_bool = turtle.attack()
    while attack_bool do
        sleep(0.5)
        attack_bool = turtle.attack()
    end
end
 
function store_shell()
    turtle.select(attack_slot)
    turtle.dropUp(1)
    turtle.dropDown(1)
end
 
function place_shulker()
    turtle.select(egg_slot)
    turtle.place()
end

function do_180()
    turtle.turnLeft()
    turtle.turnLeft()
end

function suck(num, direction)
    if direction == "top" then
        turtle.suckUp(num)
    elseif direction == "front" then
        turtle.suck(num)
    end
end

function drop(direction)
    if direction == "top" then
        turtle.dropUp()
    elseif direction == "front" then
        turtle.drop()
    end
end

function check_component(direction, num)
    chest = peripheral.wrap(direction)
    count = 0
    for i=1,27 do
        detail = chest.getItemDetail(i)
        if detail ~= nil then
            count = count + detail.count
        end
    end
    return count >= num
end

function get_component(direction, slots, num)
    if direction == "back" then
        do_180()
        new_direction = "front"
    elseif direction == "right" then
        turtle.turnRight()
        new_direction = "front"
    elseif direction == "left" then
        turtle.turnLeft()
        new_direction = "front"
    else
        new_direction = direction
    end
    ready = check_component(new_direction, num * #slots)
    if ready then
        for i, slot in ipairs(slots) do
            turtle.select(slot)
            suck(num, new_direction)
        end
    end
    if direction == "back" then
        do_180()
    elseif direction == "right" then
        turtle.turnLeft()
    elseif direction == "left" then
        turtle.turnRight()
    end
    return ready
end

function clean_slots(direction, slots)
    if direction == "back" then
        do_180()
        new_direction = "front"
    elseif direction == "right" then
        turtle.turnRight()
        new_direction = "front"
    elseif direction == "left" then
        turtle.turnLeft()
        new_direction = "front"
    else
        new_direction = direction
    end
    for i, slot in ipairs(slots) do
        turtle.select(slot)
        drop(new_direction)
    end
    if direction == "back" then
        do_180()
    elseif direction == "right" then
        turtle.turnLeft()
    elseif direction == "left" then
        turtle.turnRight()
    end
end

function craft(num)
    turtle.select(egg_slot)
    turtle.craft(num)
end

function try_to_craft(num)
    crafted = false
    ready = true
    directions = {}
    directions["right"] = {1, 3, 5, 7, 9, 11} --popped chorus
    directions["top"] = {2} --shell
    directions["back"] = {6} --chest
    directions["left"] = {10} --flower  
    for name, slots in pairs(directions) do
        success_bool = get_component(name, slots, num)
        ready = ready and success_bool
        if not ready then
            break
        end
    end
    if ready then
        craft(num)
        crafted = true
    else
        for name, slots in pairs(directions) do
            clean_slots(name, slots)
        end
    end
    return crafted
end
 
function check_slot()
    count = turtle.getItemCount(egg_slot)
    if count > 0 then
        return true
    end
    num_list = {16, 4, 1}
    for i, num in ipairs(num_list) do
        crafted = try_to_craft(num)
        if crafted then
            return true
        end
    end
    return false
end
 
function main_loop()
    continue = check_slot()
    while continue do
        place_shulker()
        kill()
        store_shell()
        continue = check_slot()
    end
end
 
main_loop()
