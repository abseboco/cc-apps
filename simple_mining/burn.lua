print("Init")

function burn_them_all()
    msg = nil
    modem = peripheral.find("modem")
    modem.open(666)
    while msg ~= 1 do
        _, _, _, _, msg, _ = os.pullEvent("modem_message")   
    end
    burn()
    modem.transmit(667, 666, 1)
    return msg
end

function burn()
    turtle.place()
    turtle.place()
    move_bool = false
    printed = false
    while not move_bool do
        move_bool = turtle.forward()
        if not printed then
            print(turtle.getFuelLevel())
            printed = true
        end
    end
end

function main_loop()
    while true do
        burn_them_all()
    end
end

main_loop()
