print("Init")

function receive_command(iter_count)
    msg = nil
    modem = peripheral.find("modem")
    modem.open(666)
    while type(msg) ~= "number" do
        _, _, _, _, msg, _ = os.pullEvent("modem_message")   
    end
    if msg == 1 then
        mine()
    end
    if msg == 0 then
        drop()
    end
    iter_count = iter_count + 1
    if iter_count >= 79 then
        refuel()
        iter_count = 0
    end
    modem.transmit(667, 666, 1)
    return iter_count
end

function mine()
    turtle.dig()
    turtle.digUp()
    turtle.digDown()
    move_bool = False
    while not move_bool do
        move_bool = turtle.forward()
        turtle.dig()
    end
end

function drop()
    local chest_slot = 16

    turtle.turnLeft()
    turtle.turnLeft()

    turtle.select(chest_slot)
    place_bool = turtle.place()
    while not place_bool do
        place_bool = turtle.place()
    end
    for i=1,15 do
        x = turtle.getItemCount(i)
        if x >= 2 then
            turtle.select(i)
            turtle.drop(x-1)
        end
    end

    finished = false
    while not finished do
        finished = check_chest()
        sleep(2)
    end

    turtle.dig()
    turtle.turnLeft()
    turtle.turnLeft()
    turtle.select(1)

    modem = peripheral.find("modem")
    modem.open(666)
    modem.transmit(667, 666, 1)
end

function check_chest()
    chest = peripheral.wrap("front")
    -- only iterate up to 15 because turtle can only have put items 
    -- in the first 15 slots of the chest
    for i=1,15 do
        tab = chest.getItemDetail(i)
        if tab ~= nil then
            if tab["count"] >= 1 then
                return false
            end
        end
    end
    return true
end

function refuel()
    num_coal = turtle.getItemCount(1)
    turtle.select(1)
    print(turtle.getFuelLevel())
    if num_coal >= 2 then
        turtle.refuel(1)
    end
    print(turtle.getFuelLevel())
    print("Refuled")
end

function main_loop()
    iter_count = 0
    while true do
        iter_count = receive_command(iter_count)
    end
end

main_loop()
