print("Init")

pp = require("cc.pretty")

local mode = "Overworld" --"Nether"
local x_pos = 0
local y_pos = 0
local z_pos = 0
local num_used_shulkers = 0
local shulker_assignment = {}

function send_command()
    modem = peripheral.find("modem")
    modem.transmit(666, 667, 0)
end

-- Movement
function go_forward(x)
    if x == nil then
        move_bool = true
        while move_bool do
            move_bool = turtle.forward()
            if move_bool then
                y_pos = y_pos + 1
            end
        end
    else
        for i=1,math.abs(x) do
            move_bool = turtle.forward()
            if move_bool then
                y_pos = y_pos + 1
            end
        end
    end
end

function go_back(x)
    if x == nil then
        move_bool = true
        while move_bool do
            move_bool = turtle.back()
            if move_bool then
                y_pos = y_pos - 1
            end
        end
    else
        for i=1,math.abs(x) do
            move_bool = turtle.back()
            if move_bool then
                y_pos = y_pos - 1
            end
        end
    end
end

function go_up(x)
    if x == nil then
        move_bool = true
        while move_bool do
            move_bool = turtle.up()
            if move_bool then
                z_pos = z_pos + 1
            end
        end
    else
        for i=1,math.abs(x) do
            move_bool = turtle.up()
            if move_bool then
                z_pos = z_pos + 1
            end
        end
    end
end

function go_down(x)
    if x == nil then
        move_bool = true
        while move_bool do
            move_bool = turtle.down()
            if move_bool then
                z_pos = z_pos - 1
            end
        end
    else
        for i=1,math.abs(x) do
            move_bool = turtle.down()
            if move_bool then
                z_pos = z_pos - 1
            end
        end
    end
end

function go_left(x)
    turtle.turnLeft()
    move_bool = true
    if x == nil then
        while move_bool do
            move_bool = turtle.forward()
            if move_bool then
                x_pos = x_pos - 1
            end
        end
    else
        for i=1,math.abs(x) do
            move_bool = turtle.forward()
            if move_bool then
                x_pos = x_pos - 1
            end
        end
    end
    turtle.turnRight()
    return move_bool
end

function go_right(x)
    turtle.turnRight()
    move_bool = true
    if x == nil then
        while move_bool do
            move_bool = turtle.forward()
            if move_bool then
                x_pos = x_pos + 1
            end
        end
    else
        for i=1,math.abs(x) do
            move_bool = turtle.forward()
            if move_bool then
                x_pos = x_pos + 1
            end
        end
    end
    turtle.turnLeft()
    return move_bool
end

function go_to_top_left()
    go_forward()
    go_left()
    go_up(4)
    go_forward(1)
    turtle.turnRight()
end

function go_back_to_start(left)
    if left == nil then
        left = true
    end
    --this specific order of x y and z movement is important for simple_mining/collect
    if left then
        turtle.turnLeft()
    end
    while y_pos > 0 do
        go_back(1)
    end
    while y_pos < 0 do
        go_forward(1)
    end
    
    while z_pos > 0 do
        go_down(1)
    end
    while z_pos < 0 do
        go_up(1)
    end
    if x_pos > 0 then
        turtle.turnLeft()
        while x_pos > 0 do
            turtle.forward()
            x_pos = x_pos - 1
        end
        turtle.turnRight()
    elseif x_pos < 0 then
        turtle.turnRight()
        while x_pos < 0 do
            turtle.forward()
            x_pos = x_pos + 1
        end
        turtle.turnLeft()
    end
end

function place_shulker(slot)
    turtle.select(slot)
    place_bool = turtle.place()
    while not place_bool do
        print("Chest placing failed")
        turtle.dig()
        place_bool = turtle.place()
    end
    your_chest = peripheral.wrap("front")
    return your_chest
end

function place_shulker_front(slot)
    turtle.select(slot)
    place_bool = turtle.place()
    while not place_bool do
        print("Chest placing failed")
        place_bool = turtle.place()
    end
end

function place_shulker_up(slot)
    turtle.select(slot)
    place_bool = turtle.placeUp()
    while not place_bool do
        print("Chest placing failed")
        place_bool = turtle.placeUp()
    end
end

function check_boxes()
    upper = peripheral.wrap("top")
    front = peripheral.wrap("front")
    for i=1,27 do
        if front ~= nil then
            item_detail = front.getItemDetail(i)
            if item_detail ~= nil then
                return false
            end
        end
        if upper ~= nil then
            item_detail = upper.getItemDetail(i)
            if item_detail ~= nil then
                return false
            end
        end
    end
    return true
end

--the rest
function material(name)
    return "minecraft:" .. name
end

function place_all_shulkers()
    iter_count = 0
    num_places = 1
    for name, list in pairs(shulker_assignment) do
        for i, entry in ipairs(list) do
            if iter_count % 2 == 1 then
                place_shulker_front(entry)
            else
                place_shulker_up(entry)
                go_left(1)
                num_places = num_places + 1
            end
            iter_count = iter_count + 1
        end
    end
    return num_places
end

function mine_all_shulkers(num_places)
    turtle.select(1)
    for i=1,num_places do
        boxes_empty = false
        while not boxes_empty do
            boxes_empty = check_boxes()
            sleep(2)
        end
        turtle.dig()
        turtle.digUp()
        go_left(1)    
    end 
end

function return_and_dump()
    go_back_to_start()
    go_right()
    num_places = place_all_shulkers()
    go_right()
    mine_all_shulkers(num_places)
    go_back_to_start(false)
end

function mine_shulker(slot)
    turtle.select(slot)
    turtle.dig()
end

function assign_new_shulker(item_name, slot)
    mine_shulker(slot)
    new_slot = update_shulker_assignment(item_name)
    your_chest = place_shulker(new_slot)
    return your_chest, new_slot
end

function get_shulker_slot(item_name)
    last_entry = #shulker_assignment[item_name]
    shulker_slot = shulker_assignment[item_name][last_entry]
    return shulker_slot
end

function get_items(item)
    their_chest = peripheral.wrap("bottom")
    --TODO dont swap boxes if item name is the same compared to previous iter
    if their_chest ~= nil then
        for i=1,15 do
            item_detail = their_chest.getItemDetail(i)
            if item_detail == nil then
                item_count = 0
                item_name = ""
            else
                item_count = item_detail.count
                item_name = item_detail.name
            end
            if item_count > 0 then
                shulker_slot = get_shulker_slot(item_name)
                your_chest = place_shulker(shulker_slot)
                num_pushed = their_chest.pushItems(peripheral.getName(your_chest), i)
                if num_pushed == 0 then
                    your_chest, shulker_slot = assign_new_shulker(item_name, shulker_slot)
                    their_chest.pushItems(peripheral.getName(your_chest), i)
                end
                mine_shulker(shulker_slot)
            end
        end
    end
end

function init_shulker_assignment(item_list)
    last_index = 0
    for i, item_name in ipairs(item_list) do
        shulker_assignment[item_name] = {i}
        last_index = i
    end
    num_used_shulkers = last_index
end

function update_shulker_assignment(item_name)
    num_used_shulkers = num_used_shulkers + 1
    last_entry = #shulker_assignment[item_name]
    shulker_assignment[item_name][last_entry + 1] = num_used_shulkers
    return num_used_shulkers
end

function debug(x)
    result = pp.pretty(x)
    print(result)
    read()
end

function collect(mode)
    if mode == "Overworld" then
        item_list = {material("coal"), 
                    material("diamond"),
                    material("raw_iron"),
                    material("raw_copper"),
                    material("lapis_lazuli"),
                    material("redstone"),
                    material("raw_gold")
        }
    elseif mode == "Nether" then
        item_list = {material("quartz"),
                    material("ancient_debris")
        }
    end
    init_shulker_assignment(item_list)

    go_to_top_left()
    for i=1,11 do
        get_items()
        go_forward(1)
    end
    get_items()
    go_right(1)
    go_down(3) --go to lower row
    go_left(1)
    for i=1,11 do
        get_items()
        go_back(1)
    end
    get_items()
    return_and_dump()
end

send_command()
collect(mode)

