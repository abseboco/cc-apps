print("Init")

function pulse()
    modem = peripheral.find("modem")
    modem.transmit(666, 667, 1)
    total_msg = 0
    modem.open(667)
    for i=1,36 do
        msg = nil
        while msg ~= 1 do
            _, _, _, _, msg, _ = os.pullEvent("modem_message")
        end 
        total_msg = total_msg + msg
    end
end

turtle.forward()
for i=1,600 do 
    print(i)
    pulse()
    move_bool = false
    printed = false
    while not move_bool do
        move_bool = turtle.forward()
        if not printed then
            print("Fuel Level", turtle.getFuelLevel())
            printed = true
        end
    end
end
